/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import java.util.Scanner;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author GeovannyRojas
 */

/*
Fuentes:
    E1:http://lineadecodigo.com/java/saber-si-existe-un-fichero-con-java/
    E2:http://chuwiki.chuidiang.org/index.php?title=Lectura_y_Escritura_de_Ficheros_en_Java
 */
public class Util {

    private Util() {
    }

    public static String desplegarMenu() {
        System.out.println("\t****Venta y Compra de vehículos****");
        System.out.println("Menú de Opciones");
        System.out.println("1. Vendedor\n"
                + "2. Comprador\n"
                + "3. Salir");
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\n");
        System.out.print("Su opción:");
        String opcion = sc.next();
        System.out.println("");
        return opcion;

    }

    public static int validarNumeros() {
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\n");
        int numero = 0;
        boolean valor = true;
        while (valor) {
            try {
                numero = Integer.parseInt(sc.next());
                if (numero >= 0) {
                    valor = false;
                } else {
                    System.out.println("No se aceptan numeros negativos.");
                    System.out.println("Ingrese el campo de nuevo.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Debe ingresar numeros.");
            }
        }
        return numero;
    }

    public static String validarLetras() {
        Boolean valor2 = true;
        Scanner sc = new Scanner(System.in);
        String palabra = null;
        while (valor2) {
            palabra = sc.next();
            int contador = 0;
            char[] aCaracteres = palabra.toCharArray();
            for (int i = 0; i < palabra.length(); i++) {
                String p = Character.toString(aCaracteres[i]);
                try {
                    int numero = Integer.parseInt(p);
                    contador++;
                } catch (NumberFormatException e) {
                }
            }
            if (contador != 0) {
                System.out.println("Recuerde! No ingrese datos numericos.");
                System.out.println("Ingrese el campo solo con letras:");
            } else {
                valor2 = false;
            }
        }
        return palabra;
    }

    public static void enviarGmail(String destinatario, String asunto, String cuerpo) {
        String remitente = "vehiculosseg";
        Properties props = System.getProperties();
        props.put("mail.smtp.host", "smtp.gmail.com");  //El servidor SMTP de Google
        props.put("mail.smtp.user", remitente);
        props.put("mail.smtp.clave", "Vehiculos123");    //La clave de la cuenta
        props.put("mail.smtp.auth", "true");    //Usar autenticación mediante usuario y clave
        props.put("mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", "587"); //El puerto SMTP seguro de Google

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(remitente));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));   //Se podrían añadir varios de la misma manera
            message.setSubject(asunto);
            message.setText(cuerpo);
            try (Transport transport = session.getTransport("smtp")) {
                transport.connect("smtp.gmail.com", remitente, "Vehiculos123");
                transport.sendMessage(message, message.getAllRecipients());
            }
        } catch (MessagingException me) {
            //Si se produce un error
        }
    }

    // Codigo de encriptado, entendi la mitad (borrar este cmn JAJA)
    public static byte[] getSHA(String input) throws NoSuchAlgorithmException {
        // Static getInstance method is called with hashing SHA  
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        // digest() method called  
        // to calculate message digest of an input  
        // and return array of byte 
        return md.digest(input.getBytes(StandardCharsets.UTF_8));
    }

    public static String toHexString(byte[] hash) {
        // Convert byte array into signum representation  
        BigInteger number = new BigInteger(1, hash);

        // Convert message digest into hex value  
        StringBuilder hexString = new StringBuilder(number.toString(16));

        // Pad with leading zeros 
        while (hexString.length() < 32) {
            hexString.insert(0, '0');
        }

        return hexString.toString();

    }

    public static String codifica(String dato) {
        String hash = "";
        try {
            hash = toHexString(getSHA(dato));
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception thrown for incorrect algorithm: " + e);
        } finally {
            return hash;
        }
    }

    //Escritura de datos
    public static void Escribe(String cadena, String archivo) {
        String ruta = System.getProperty("user.dir") + "\\Dts\\" + archivo;
        FileWriter fw = null;
        PrintWriter salida = null;
        try {
            fw = new FileWriter(ruta, true);
            salida = new PrintWriter(fw);
            salida.println(cadena);
            salida.flush();
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("Error archivo");
        }
    }

    //Lectura de datos
    public static String[] Lee(String archivo, String identificador, int posi) {            //Recibe el nombre del archivo, y un identificador en el archivo para traer los elementos asignados al mismo
        String[] credenciales = null;
        String ruta = System.getProperty("user.dir") + "\\Dts\\" + archivo;
        File arch = new File(ruta);         //E1
        if (arch.exists()) {
            FileReader fr = null;

            try {
                fr = new FileReader(ruta);
                BufferedReader entrada = new BufferedReader(fr);
                String cadena = entrada.readLine();
                while (cadena != null) {
                    if (cadena.split(",")[posi].equals(identificador)) {
                        credenciales = cadena.split(",");
                    }
                    cadena = entrada.readLine();

                }
            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            } finally {
                try {
                    if (fr != null) {
                        fr.close();
                    }
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                } finally {
                    return credenciales;
                }
            }
        } else {
            return null;
        }
    }

    public static int lineanum(String archivo, String identificador, int posi) throws IOException {       //Cuenta la cantidad de veces que existe un elemento en un archivo en base a su identificador
        int conta = 0, linea = -1;
        String ruta = System.getProperty("user.dir") + "\\Dts\\" + archivo;
        File arch = new File(ruta);         //E1
        if (arch.exists()) {
            FileReader fr = null;
            try {
                fr = new FileReader(ruta);
                BufferedReader entrada = new BufferedReader(fr);
                String cadena = entrada.readLine();
                while (cadena != null) {
                    if (cadena.split(",")[posi].equals(identificador)) {
                        linea = 1;
                    }
                    conta += 1;
                    cadena = entrada.readLine();
                }
            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            } finally {
                return linea;
            }
        } else {
            return 0;
        }

    }

    public static Double[] obtenerRangoD(String cadena) {
        String[] rango;
        String ingreso;
        Double[] resu = new Double[2];
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\n");
        do {
            System.out.print(cadena);
            ingreso = sc.next();
            if (!(ingreso.equals(""))){
                rango=ingreso.split(" ");
                try {
                    if (rango.length == 4) {
                        resu[0] = Double.parseDouble(rango[1]);
                        resu[1] = Double.parseDouble(rango[3]);
                    } else {
                        resu[0] = Double.parseDouble("gg");
                    }
                } catch (NumberFormatException a) {
                    System.out.println("Ingrese un rango valido.");
                    rango = " ".split("");
                }
            }else{
                resu[0]=0.0;
                resu[1]=999999999999.0;
                rango="a b c b".split(" ");
            }
        } while (rango.length != 4 || rango.length == 0);
        return resu;
    }

    public static Integer[] obtenerRangoI(String cadena) {
        String[] rango;
        String ingreso;
        Integer[] resu = new Integer[2];
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\n");
        do {
            System.out.print(cadena);
            ingreso = sc.next();
            if(!(ingreso.equals(""))){
                rango= ingreso.split(" ");
                try {
                    if (rango.length == 4) {
                        resu[0] = Integer.parseInt(rango[1]);
                        resu[1] = Integer.parseInt(rango[3]);
                    } else {
                        resu[0] = Integer.parseInt("gg");
                    }
                } catch (NumberFormatException a) {
                    System.out.println("Ingrese un rango valido.");
                    rango = " ".split("");
                }                                           
            }else{
                resu[0]=0;
                resu[1]=999999999;
                rango="a b c d".split(" ");
            }
        } while (rango.length != 4 || rango.length == 0);
        return resu;
    }
}
