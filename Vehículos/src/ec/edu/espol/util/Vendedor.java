/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import static ec.edu.espol.util.Util.validarLetras;
import static ec.edu.espol.util.Util.validarNumeros;
import static ec.edu.espol.util.Vehiculos.verificarPlaca;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author GeovannyRojas
 */

/*
    Links de referencia: https://www.it-swarm.dev/es/java/como-leer-una-linea-especifica/968475320/
                         https://es.stackoverflow.com/questions/29419/cómo-sobreescribir-una-línea-de-un-archivo-txt-en-java
 */
public class Vendedor extends Persona {

    private ArrayList<Venta> listaVenta;
    private ArrayList<Oferta> listaOferta;

    public Vendedor(String nombres, String apellidos, String organizacion, String identificador_cuenta, Cuenta cuenta, String cedula) {
        super(nombres, apellidos, organizacion, identificador_cuenta, cuenta, cedula);
        this.listaVenta = new ArrayList<>();
    }

    public ArrayList<Venta> getListaVenta() {
        return listaVenta;
    }

    public static String menuVendedor() {
        System.out.println("\n****MENÚ DEL VENDEDOR****");
        System.out.println("1. Registrar un Nuevo Vendedor\n"
                + "2. Ingresar un Nuevo Vehículo\n"
                + "3. Aceptar Oferta\n"
                + "4. Regresar");
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\n");
        System.out.print("Su opción:");
        String opcion = sc.next();
        System.out.println("");
        return opcion;
    }

    public static String presentarVehiculos() {
        Scanner sc = new Scanner(System.in);
        String vehi = "";
        boolean auto;
        boolean camioneta;
        boolean moto;
        boolean camion;

        do {
            System.out.println("-Auto\n"
                    + "-Camioneta\n"
                    + "-Motocicleta\n"
                    + "-Camion\n"
                    + "¿Qué tipo de vehículo desea registrar?");
            vehi = sc.nextLine();
            auto = vehi.equalsIgnoreCase("Auto");
            camioneta = vehi.equalsIgnoreCase("Camioneta");
            moto = vehi.equalsIgnoreCase("Motocicleta");
            camion = vehi.equalsIgnoreCase("Camion");
        } while ((auto || camioneta || moto || camion) == false);

        return vehi;
    }

    public static Venta registrarVehiculo(String vehiculo, String usuario) throws IOException {
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\n");
        sc.useLocale(Locale.US);
        String placa;
        String motor;
        String color;
        String combustible;
        int recorrido;
        String marca;
        int año;
        double precio;
        Vehiculos vehi;
        Venta venta;
        String vidrio;

        System.out.println("REGISTRO DE VEHICULO");
        do {
            System.out.println("Ingrese la placa");
            placa = sc.next().toUpperCase();
        } while (verificarPlaca(placa));
        System.out.println("Ingrese la marca");
        marca = validarLetras(); //solo alfanumerico:(
        System.out.println("Ingrese el motor");
        motor = sc.next();
        System.out.println("Ingrese el color");
        color = validarLetras();
        System.out.println("Ingrese que combustible usa");
        combustible = sc.next();
        System.out.println("Ingrese el recorrido");
        recorrido = validarNumeros();
        System.out.println("Ingrese el año");
        año = validarNumeros();
        String dir = System.getProperty("user.dir") + "\\Dts\\";
        if (vehiculo.equalsIgnoreCase("Auto")) {
            System.out.println("Usa vidrios electricos o mecánicos: ");
            vidrio = sc.next();
            vehi = new Auto(placa, motor, color, combustible, recorrido, marca, año, vidrio);
            Auto auto = (Auto) vehi;
            auto.registrar(dir + "Autos.txt");
        } else if (vehiculo.equalsIgnoreCase("Motocicleta")) {
            vehi = new Motocicletas(placa, motor, color, combustible, recorrido, marca, año);
            Motocicletas moto = (Motocicletas) vehi;
            moto.registrar(dir + "Motos.txt");
        } else if (vehiculo.equalsIgnoreCase("Camioneta")) {
            System.out.println("Usa vidrios electricos o mecánicos: ");
            vidrio = sc.next();
            vehi = new Camionetas(placa, motor, color, combustible, recorrido, marca, año, vidrio);
            Camionetas camioneta = (Camionetas) vehi;
            camioneta.registrar(dir + "Camionetas.txt");
        } else if (vehiculo.equalsIgnoreCase("Camion")) {
            System.out.println("Usa vidrios electricos o mecánicos: ");
            vidrio = sc.next();
            vehi = new Camion(placa, motor, color, combustible, recorrido, marca, año, vidrio);
            Camion camion = (Camion) vehi;
            camion.registrar(dir + "Camiones.txt");
        } else
            vehi = null;
        System.out.println("Ingrese el precio a vender $$$");
        precio = validarNumeros();

        venta = new Venta(usuario, vehi.getPlaca(), precio);

        return venta;

    }

    public static Vendedor crearPersonaVendedor(String archivo, String usuario, Cuenta cuenta) {
        String[] credenciales = null;
        String ruta = System.getProperty("user.dir") + "\\Dts\\" + archivo;
        File arch = new File(ruta);         //E1
        if (arch.exists()) {
            FileReader fr = null;

            try {
                fr = new FileReader(ruta);
                BufferedReader entrada = new BufferedReader(fr);
                String cadena = entrada.readLine();
                Vendedor v;
                while (cadena != null) {
                    credenciales = cadena.split(",");
                    if (usuario.equals(credenciales[0])) {
                        v = new Vendedor(credenciales[1], credenciales[2], credenciales[3], credenciales[4], cuenta, credenciales[5]);
                        return v;
                    }
                    cadena = entrada.readLine();
                }

            } catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                System.out.println(e.getMessage());
            } finally {
                try {
                    if (fr != null) {
                        fr.close();
                    }
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        } else {
            return null;
        }
        return null;
    }

    public void leerVenta(String archivo) {
        try (Scanner sc = new Scanner(new File(archivo))) {
            while (sc.hasNextLine()) {
                String[] id = sc.nextLine().split(",");
                if (id[0].equals(this.getCuenta().getUsuario())) {
                    this.listaVenta.add(new Venta(id[0], id[1], Double.parseDouble(id[2]))); //Necesitamos averiguar de que placa es el auto.
                }
            }
        } catch (Exception e) {
            System.out.println("--No se han registrado ventas en este momento--\n");
            this.listaVenta=null;
        }
    }

    public void mostrarOfertas(ArrayList<Vehiculos> listav) throws FileNotFoundException {
        String placa;
        String opcion;
        String vehiculo = null;
        Scanner sc = new Scanner(System.in);
        if(this.listaVenta!=null){
            try{
                System.out.println("Ingrese placa");
                placa = sc.next().toUpperCase();
                for (Venta v : this.listaVenta) {
                    if (placa.equals(v.getPlaca())) {
                        this.listaOferta = Oferta.listaOferta(v.getPlaca());
                    }
                }
                Oferta v;
                String cadena;
                sc.useDelimiter("\n");
                int i = 0;
                for (Vehiculos vq : listav) {
                    if (placa.equals(vq.getPlaca())) {
                        System.out.print("Marca: " + vq.getMarca());
                        vehiculo = vq.mostrar();
                    }
                }
                System.out.println("\tPrecio: $" + Util.Lee("Ventas.txt", placa, 1)[2]);
                System.out.println("Se han realizado " + this.listaOferta.size() + " ofertas");
                while (i < this.listaOferta.size()) {
                    System.out.println((i + 1) + ") Oferta :");
                    System.out.println("Correo del ofertante: " + this.listaOferta.get(i).getCorreo());
                    System.out.println("Precio Ofertado: " + this.listaOferta.get(i).getMontoOfertado());

                    if(listaOferta.size()==1)
                    {
                        System.out.println("\n1.- Aceptar Oferta");
                        System.out.println("2.- Salir");
                        opcion = sc.next();
                        if (opcion.equals("1")) {
                            aceptarOferta(i);
                            System.out.println("Enviando correo, espere por favor....");
                            Util.enviarGmail(listaOferta.get(i).getCorreo(), "Oferta de Vehiculo", "Su oferta sobre este vehículo\n\n"+vehiculo+"\n\n Ha sido aceptada\nFavor comunicarse con: "+this.getCuenta().getCorreo()+"\n Gracias por preferirnos.");
                            try {
                                sleep(1000);
                            } catch (InterruptedException ex) {
                            }
                            System.out.println("Correo enviado.");
                            i = this.listaOferta.size();
                        }
                        if (opcion.equals("2")) {
                            i = this.listaOferta.size();
                        }

                    }

                    else if (i == this.listaOferta.size() - 1 && i != 0) {
                        System.out.println("\n1.- Anterior Oferta");
                        System.out.println("2.- Aceptar oferta");
                        System.out.println("3.- Salir");
                        System.out.println("Ingrese su elección:");
                        opcion = sc.next();
                        if (opcion.equals("1")) {
                            i--;
                        }
                        else if (opcion.equals("2")) {
                            aceptarOferta(i);
                            System.out.println("Enviando correo, espere por favor....");
                            Util.enviarGmail(listaOferta.get(i).getCorreo(), "Oferta de Vehiculo", "Su oferta sobre este vehículo\n\n"+vehiculo+"\n\n Ha sido aceptada\nFavor comunicarse con: "+this.getCuenta().getCorreo()+"\n Gracias por preferirnos.");
                            try {
                                sleep(1000);
                            } catch (InterruptedException ex) {
                            }
                            System.out.println("Correo enviado.");
                            i = this.listaOferta.size();
                        }
                        else if (opcion.equals("3"))
                            i = this.listaOferta.size();
                    }
                    else if(i<1 && this.listaOferta.size()>=2)
                    {
                        System.out.println("\n1.- Siguiente Oferta");
                        System.out.println("2.- Aceptar Oferta");
                        System.out.println("3.- Salir");
                        System.out.println("Ingrese su eleccion: ");
                        String opci = sc.next();
                        if(opci.equals("1"))
                            i++;
                        if(opci.equals("2"))
                        {
                            aceptarOferta(i);
                            System.out.println("Enviando correo, espere por favor....");
                            Util.enviarGmail(listaOferta.get(i).getCorreo(), "Oferta de Vehiculo", "Su oferta sobre este vehículo\n\n"+vehiculo+"\n\n Ha sido aceptada\nFavor comunicarse con: "+this.getCuenta().getCorreo()+"\n Gracias por preferirnos.");  
                             try {
                                sleep(1000);
                            } catch (InterruptedException ex) {
                            }
                            System.out.println("Correo enviado.");
                            i = this.listaOferta.size();
                        }
                        if(opci.equals("3"))
                            i = this.listaOferta.size();



                    }
                    else if (i <=2 && i<this.listaOferta.size()-1) {
                        System.out.println("\n1.- Siguiente Oferta");
                        System.out.println("2.- Anterior oferta");
                        System.out.println("3.- Aceptar Oferta");
                        System.out.println("4.- Salir");

                        System.out.println("Ingrese su eleccion: ");
                        String opci = sc.next();

                        if (opci.equals("1")) {
                            i++;
                        }
                        if (opci.equals("2")) {
                            i--;

                        }
                        if (opci.equals("3")) {
                            aceptarOferta(i);
                            System.out.println("Enviando correo, espere por favor....");
                            Util.enviarGmail(listaOferta.get(i).getCorreo(), "Oferta de Vehiculo", "Su oferta sobre este vehículo\n\n"+vehiculo+"\n\n Ha sido aceptada\nFavor comunicarse con: "+this.getCuenta().getCorreo()+"\n Gracias por preferirnos.");
                             try {
                                sleep(1000);
                            } catch (InterruptedException ex) {
                            }
                            System.out.println("Correo enviado.");
                            i = this.listaOferta.size();
                        }
                        if(opci.equals("4"))
                            i = this.listaOferta.size();
                    }
                }
            }catch(NullPointerException ne){
                System.out.println("ATENCIÓN: No hay registros de la placa ingresada\n");
            }
        }else {
            System.out.println("ATENCIÓN: No hay ventas activas del usuario: "+super.getCuenta().getUsuario()+" \n");
        }
    }

    public void aceptarOferta(int pos) throws FileNotFoundException {
        StringBuilder archivo = new StringBuilder();
        Oferta datos = this.listaOferta.get(pos);
        try {
            String respaldo = "";
            String[] ventadatos = Util.Lee("Ventas.txt", datos.getPlaca(), 1);
            //sobre.writeBytes(ventadatos[0]+","+ventadatos[1]+","+ventadatos[2]+","+"true");
            BufferedReader file = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\Dts\\Ventas.txt"));
            String line;
            String input = "";
            while ((line = file.readLine()) != null) {
                /* Podemos verificar si es Usuario_1 y \r\n es para hacer el 
                  Salto de Línea y tener el formato original */
                if (line.contains(ventadatos[1])) {
                    archivo.append(line.replaceFirst("false", "true"));
                    archivo.append("\n");
                } else {
                    archivo.append(line + "\r\n");
                }
            }
            FileOutputStream fileOut = new FileOutputStream(System.getProperty("user.dir") + "\\Dts\\Ventas.txt");
            fileOut.write(archivo.toString().getBytes());
            fileOut.close();
        } catch (Exception e) {
            System.out.println();
        }

    }


}
