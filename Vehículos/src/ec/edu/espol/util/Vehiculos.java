/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
/**
 *
 * @author VehiculosSeg
 */
public abstract class Vehiculos {
    
    protected String placa;
    protected String motor;
    protected String color; 
    protected String combustible;
    protected int recorrido;
    protected String marca;
    protected int año;

    public Vehiculos()
    {
        this.placa ="No hay";
    }
    
    public Vehiculos(String placa, String motor, String color, String combustible, int recorrido, String marca, int año){
        this.placa = placa;
        this.motor = motor;
        this.color = color;
        this.combustible = combustible;
        this.recorrido = recorrido;
        this.marca = marca;
        this.año = año;
    }    
    
    public String getPlaca() {
        return placa;
    }

    public String getMotor() {
        return motor;
    }

    public String getColor() {
        return color;
    }

    public String getCombustible() {
        return combustible;
    }

    public int getRecorrido() {
        return recorrido;
    }

    public String getMarca() {
        return marca;
    }

    public int getAño() {
        return año;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setCombustible(String combustible) {
        this.combustible = combustible;
    }

    public void setRecorrido(int recorrido) {
        this.recorrido = recorrido;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setAño(int año) {
        this.año = año;
    }
    
    @Override
    public String toString()
    {
        return (this.placa+","+this.marca+","+this.motor+","+this.color+","+this.combustible+","+this.recorrido+","+this.año);
    }

    public abstract String mostrar();
    
    public void registrar(String archivo){
    try(FileWriter fw = new FileWriter(archivo, true);
        BufferedWriter bw = new BufferedWriter(fw);
        PrintWriter out = new PrintWriter(bw)){
            out.println(this.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    public static boolean verificarPlaca(String placa) throws IOException {
        String ruta = System.getProperty("user.dir") + "\\Dts\\Ventas.txt";
        File fichero = null;
        FileReader fr = null;
        BufferedReader br = null;
        File arch = new File(ruta);
        if (arch.exists()) {
            try {
                // Apertura del fichero y creacion de BufferedReader para poder
                // hacer una lectura comoda (disponer del metodo readLine()).
                fichero = new File(ruta);
                fr = new FileReader(fichero);
                br = new BufferedReader(fr);
    
                // Lectura del fichero
                String linea;
                String[] contenido = null;
                while ((linea = br.readLine()) != null) {
                    contenido = linea.split(",");
                    if (placa.equals(contenido[1])) {
                        return true;
                    } else {
                        return false;
                    }

                }
                fr.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return false;
        }
        return false;
    }
    
 }
