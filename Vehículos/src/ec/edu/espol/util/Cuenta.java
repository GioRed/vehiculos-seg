/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import static ec.edu.espol.util.Util.Lee;
import static ec.edu.espol.util.Util.codifica;
import java.io.File;
import java.util.Objects;
import java.util.Scanner;
/**
 *
 * @author GeovannyRojas
 */
public class Cuenta {
    private String usuario;
    private String contraseña;
    private String correo;
    private String tipocuenta;

    public Cuenta(String usuario, String contraseña, String correo, String tipocuenta) {
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.correo = correo;
        this.tipocuenta = tipocuenta;
    }

    public Cuenta() {
    }
    
    public String getTipocuenta() {
        return tipocuenta;
    }
    
    public String getUsuario() {
        return usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public String getCorreo() {
        return correo;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setTipocuenta(String tipocuenta) {
        this.tipocuenta = tipocuenta;
    }
    
    public void registrar(){
        
        String datos=this.usuario+","+this.contraseña+","+this.correo+","+this.tipocuenta+"";
        Util.Escribe(datos,"Cuentas.txt");
  
    }
    public static Cuenta ValidaSesion() {
        Cuenta usuario = null;
        boolean flag = true;
        String ruta = System.getProperty("user.dir") + "\\Dts\\Cuentas.txt";
        File arch = new File(ruta);
        if (arch.exists()) {
            String[] credenciales;
            String user, contraseña;
            Scanner sc = new Scanner(System.in);
            do {
                System.out.println("Usuario: ");
                user = sc.next().toLowerCase();
                credenciales = Lee("Cuentas.txt", user, 0);
                System.out.println("Contraseña: ");
                contraseña = codifica(sc.next());
                if (credenciales != null) {
                    if (credenciales[1].equals(contraseña)) {
                        flag = false;
                        usuario = new Cuenta();
                        usuario.setUsuario(credenciales[0]);
                        usuario.setContraseña(credenciales[1]);
                        usuario.setCorreo(credenciales[2]);
                        usuario.setTipocuenta(credenciales[3]);
                    }
                }
                if (flag) {
                    System.out.println("Credenciales incorrectas.");
                    System.out.println("Desea retroceder? S/N");
                    String boli = sc.next().toUpperCase();
                    if (boli.equals("S")) {
                        flag = false;
                    }
                }
            } while (flag);
        }
        else
            System.out.println("ATENCIÓN: No hay ninguna cuenta registrada\n");
        return usuario;
    }
    
     public static boolean esCorreo(String correo) {
        int arroba = correo.indexOf("@");
        int com = correo.indexOf(".com");
        int es = correo.indexOf(".es");
        int ec = correo.indexOf(".ec");

        if (arroba != -1 && (com != -1 || es!= -1 || ec!= -1)) 
        {
            return true;
        } else {
            return false;
        }
    }
     
    @Override
    public boolean equals(Object o)
    {
      if(o==null || this.getClass()!=o.getClass())
          return false;
      if(this==o)
          return true;
      
      Cuenta other = (Cuenta)o;
      
      return(Objects.equals(this.usuario,other.usuario));
    }
}
