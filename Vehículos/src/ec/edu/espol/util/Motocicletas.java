/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author GeovannyRojas
 */
public class Motocicletas extends Vehiculos{
    
    public Motocicletas(String placa,String motor,String color,String combustible,int recorrido,String marca,int año)
    {   super();
        this.placa = placa;
        this.motor = motor;
        this.color = color;
        this.combustible = combustible;
        this.recorrido = recorrido;
        this.marca = marca;
        this.año = año;
        
    }
    @Override
    public String mostrar()
    {
        return "Marca: "+this.marca+
        "\nAño: "+this.año+
        "\nRecorrido: "+this.recorrido+
        "\nColor: "+this.color+
        "\nCombustible: "+this.combustible+
        "\nPlaca: "+this.placa+
        "\nPrecio: $"+Util.Lee("Ventas.txt", this.placa,1)[2];
       
    }
    
     public static ArrayList<Vehiculos> leerMotos()
    {   
        String archivo = System.getProperty("user.dir")+"\\Dts\\"+"Motos.txt";
        ArrayList<Vehiculos> motos = new ArrayList<>();
        try(Scanner sc = new Scanner(new File(archivo))){
            while(sc.hasNextLine()){
                String[] id = sc.nextLine().split(",");
                if(Util.Lee("Ventas.txt",id[0], 1)[3].contains("false")){
                    Vehiculos vehi = new Motocicletas(id[0],id[1],id[2],id[3],Integer.parseInt(id[4]),id[5],Integer.parseInt(id[6]));
                    motos.add(vehi);
                }                
            }
        }
        catch(Exception e){
            FileReader fr = null;
        }
        return motos;
    }
    
    @Override
    public String toString()
    {
     
        return (this.placa+","+this.motor+","+this.color+","+this.combustible+","+this.recorrido+","+this.marca+","+this.año);
    }
    
}
