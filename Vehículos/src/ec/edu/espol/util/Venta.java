/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import ec.edu.espol.util.*;

/**
 *
 * @author GeovannyRojas
 */
public class Venta {
    private Vehiculos vehiculo;
    private String placa;
    private String usuario;
    private boolean estado;
    private double precio;
    private Comprador comprador;
    
    
    public Venta(String usuario, String placa, double precio)
    {
        this.usuario = usuario;
        this.precio = precio;
        this.placa = placa;
        this.estado=false;
        
    }
    
    public Vehiculos getVehiculo() {
        return vehiculo;
    }

    public String getPlaca() {
        return placa;
    }

    public String getUsuario() {
        return usuario;
    }

    public boolean isEstado() {
        return estado;
    }

    public double getPrecio() {
        return precio;
    }

    public Comprador getComprador() {
        return comprador;
    }
    
     public static void registrarVenta(Venta venta) throws IOException
     {
        
        try (FileWriter ficheroventas = new FileWriter(System.getProperty("user.dir")+"\\Dts\\"+"Ventas.txt",true)) {        
            ficheroventas.append(venta.toString()+"                       \n"); //usuario,placa y precio
            ficheroventas.close();
        }
        catch(Exception e)
        {
            e.getMessage();
        }
        
    }         
    public static ArrayList<Vehiculos> vincularTodosLosVehiculos()
    {   ArrayList<Vehiculos> todosLosVehiculos = new ArrayList<>();
        ArrayList<Vehiculos> autos = Auto.leerAutos();
        for(Vehiculos v:autos)
            todosLosVehiculos.add(v);
        ArrayList<Vehiculos> camion = Camion.leerCamiones();
        for(Vehiculos c:camion)
            todosLosVehiculos.add(c);
        ArrayList<Vehiculos> camionetas = Camionetas.leerCamionetas();
        for(Vehiculos camioneta:camionetas)
            todosLosVehiculos.add(camioneta);
        ArrayList<Vehiculos> motos = Motocicletas.leerMotos();
        for(Vehiculos m:motos)
            todosLosVehiculos.add(m);
        
        return todosLosVehiculos;
    }
    
    @Override 
    public String toString()
    {
        return (this.usuario+","+this.placa+","+this.precio+","+this.estado);
    }
}
