/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author GeovannyRojas
 */
public class Comprador extends Persona {
    private ArrayList<Oferta> listaOfertas;
    
    public Comprador(String nombres, String apellidos, String organizacion, String identificador_cuenta, Cuenta cuenta, String cedula) {
        super(nombres, apellidos, organizacion, identificador_cuenta, cuenta, cedula);
        this.listaOfertas = new ArrayList<>();
    }
    
    public void llenarLista(Oferta oferta)
    {
        listaOfertas.add(oferta);
    }
    
    public ArrayList<Oferta> getListaOferta() {
        return listaOfertas;
    }
    
    public static String menuComprador()
    {
        System.out.println("****MENÚ DEL COMPRADOR****\n"+
                           "1. Registrar comprador\n"+
                           "2. Ofertar por un vehículo\n"+
                           "3. Regresar");
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\n");
        System.out.print("Su opción:");
        String opcion = sc.next();
        System.out.println("");
        return opcion;
    }
    
     public static String mostrarVehiculos()
    {
        Scanner sc = new Scanner(System.in);
        String vehi;
        boolean auto;
        boolean camioneta;
        boolean moto;
        boolean camion;
        
        do{
        System.out.println("-Auto\n"+
                           "-Camioneta\n"+
                           "-Motocicleta\n"+
                           "-Camion\n"+
                           "¿Qué tipo de vehículo anda buscando (Si desea ver todos los tipos pulse \"Enter\"))?");
        vehi = sc.nextLine();
        auto = vehi.equalsIgnoreCase("Auto");
        camioneta = vehi.equalsIgnoreCase("Camioneta");
        moto = vehi.equalsIgnoreCase("Motocicleta");
        camion = vehi.equalsIgnoreCase("Camion");
        }while((auto || camioneta|| moto ||camion)==false && vehi.equals("")==false);
        
        return vehi;
    }
     
     public static Comprador crearPersonaComprador(String archivo,String usuario,Cuenta cuenta)
    {
        String [] credenciales = null;
        String ruta=archivo;  
        File arch = new File(ruta);         //E1
        if(arch.exists()){
            FileReader fr = null;
            Comprador v=null;
            try {
                fr = new FileReader(ruta);
                BufferedReader entrada = new BufferedReader(fr);
                String cadena = entrada.readLine(); 
                
                while (cadena != null) {               
                       credenciales=cadena.split(",");
                       if(usuario.equals(credenciales[0])){
                           v = new Comprador(credenciales[1],credenciales[2],credenciales[3],credenciales[4],cuenta,credenciales[5]);
                       }
                       cadena=entrada.readLine();
                    }
                    
                   
                }
            catch (FileNotFoundException e) {
                System.out.println(e.getMessage());
            } finally {
                try {
                    if (fr != null) {
                        fr.close();
                    }
                } catch (IOException e) {
                    System.out.println(e.getMessage());                                                               
                }finally{
                    return v;
                }
              }
            }
        else
            return null;
    }

     public static Oferta registrarOfertaVeh(Vehiculos vehiculo, String correo) 
    {   
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\n");
        
        String placa = vehiculo.getPlaca();
        double precioOferta;
        Oferta oferta;
        
        System.out.println("REGISTRO DE OFERTA");
        System.out.println("Ingrese el valor ofertado");
        precioOferta = sc.nextDouble();
        
        oferta = new Oferta(placa,precioOferta,correo);
        return oferta;
        
    } 
}
