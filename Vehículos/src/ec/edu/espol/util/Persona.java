/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;
import static ec.edu.espol.util.Util.Lee;
import static ec.edu.espol.util.Util.codifica;
import static ec.edu.espol.util.Util.validarLetras;
import java.util.Objects;
import java.util.Scanner;

/*Referencias :
    ref1:
    https://www.campusmvp.es/recursos/post/java-como-listar-filtrar-y-obtener-informacion-de-carpetas-y-archivos.aspx
    https://stackoverflow.com/questions/7347856/how-to-convert-a-string-into-an-arraylist
*/
/**
 *
 * @author GeovannyRojas
 */
public class Persona {
  
    protected String nombres;
    protected String apellidos;
    protected String organizacion;
    protected String identificador_cuenta;
    protected Cuenta cuenta;
    protected String cedula;

    public Persona(String nombres, String apellidos, String organizacion, String identificador_cuenta, Cuenta cuenta, String cedula) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.organizacion = organizacion;
        this.identificador_cuenta = identificador_cuenta;
        this.cuenta = cuenta;
        this.cedula = cedula;
    }
    
    public String getIdentificador_cuenta() {
        return identificador_cuenta;
    }
    
    
    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public String getCedula() {
        return cedula;
    }


    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void setIdentificador_cuenta(String identificador_cuenta) {
        this.identificador_cuenta = identificador_cuenta;
    }
  
    

    public void registrar(){ 
        String datos=this.getCuenta().getUsuario()+","+this.nombres+","+this.apellidos+","+this.organizacion+","+this.identificador_cuenta+","+this.cedula+"";
        Util.Escribe(datos,"personas.txt");

    }
    public static Persona registro(int tipousuario) {
        String[] credenciales;
        String correo, usuario, contraseña, organizacion, nombres, apellidos, cedula;
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\n");
        do {
            System.out.println("Ingrese un nombre de usuario: ");
            usuario = sc.nextLine().toLowerCase();
            credenciales = Lee("Cuentas.txt", usuario, 0);
            if (credenciales != null) 
                System.out.println("Este usuario ya existe.");
        } while (credenciales != null);
        System.out.println("Ingrese su contraseña");
        contraseña = codifica(sc.next());
        do {
            System.out.println("Ingrese su correo: ");
            correo = sc.next().toLowerCase();
            credenciales = Lee("Cuentas.txt", correo, 2);
            if (credenciales != null) 
                System.out.println("Este correo ya se encuentra registrado, favor ingresar otro correo.");
        } while (Cuenta.esCorreo(correo) == false || correo.length() < 10 || credenciales!=null);
        System.out.println("Ingrese sus nombres: ");
        nombres = validarLetras();
        System.out.println("Ingrese sus apellidos: ");
        apellidos = validarLetras();
        System.out.println("Ingrese su número de cédula: ");
        cedula = sc.next();
        System.out.println("Ingrese su organización: ");
        organizacion = validarLetras();
        Cuenta datos = new Cuenta(usuario, contraseña, correo, tipousuario + ""); // debe existir algo que me diga si es comprador o vndedor, es decir, el tipo de cuenta
        datos.registrar();
        Persona user = new Persona(nombres, apellidos, organizacion, "1", datos, cedula); // puse public los datos de cuenta porque no podia acceder a sus atributos, y creo que eso querian hacer
        user.registrar();
        System.out.println("\n**Se ha registrado con éxito**\n");
        return user;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vendedor other = (Vendedor) obj;
        if (!Objects.equals(this.cedula, other.cedula)) {
            return false;
        }
        return true;
    }
}
