/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author GeovannyRojas
 */
public class Oferta {

    private String placa;

    public String getPlaca() {
        return placa;
    }

    public double getMontoOfertado() {
        return montoOfertado;
    }

    public String getCorreo() {
        return correo;
    }
    private double montoOfertado;
    private String correo;

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setMontoOfertado(double montoOfertado) {
        this.montoOfertado = montoOfertado;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    
    public Oferta(String placa, double montoOfertado, String correo) {
        this.placa = placa;
        this.montoOfertado = montoOfertado;
        this.correo = correo;
    }

    
    public static void registrarOferta(Oferta oferta) throws IOException {

        try (FileWriter ficherofertas = new FileWriter(System.getProperty("user.dir")+"Ofertas.txt", true)) {
            ficherofertas.append(oferta.toString() + "\n"); //correo,precio ofertado
            ficherofertas.close();
        } catch (Exception e) {
            e.getMessage();
        }

    }
    
    
    public static ArrayList<Oferta> listaOferta(String placa){
        ArrayList<Oferta> lista= new ArrayList<>();
        try(Scanner sc = new Scanner(new File(System.getProperty("user.dir")+"\\Dts\\"+"Ofertas.txt"))){
            while(sc.hasNextLine()){
                String[] id = sc.nextLine().split(",");
                if(id[0].equals(placa)){
                    lista.add(new Oferta(id[0],Double.parseDouble(id[1]),id[2])); //Necesitamos averiguar de que placa es el auto.
                }
            }
        }
        catch(Exception e){
            System.out.println("\nNo se han encontrado ofertas para este vehículo, pruebe en otro momento\n");
        }finally{
            return lista;
        }
    }
    
    
    
    public static void vehiculosEnVenta(ArrayList<Vehiculos> lista,String correo)
    {   Vehiculos v;
        String cadena;
        String opcion;
        Scanner sc = new Scanner(System.in);
        sc.useDelimiter("\n");
        int i=0;
        while(i<lista.size()){
            System.out.println("\n"+(i+1)+") En Venta");
            v = lista.get(i);
            System.out.println(v.mostrar());
            
            if(lista.size()==1){
                System.out.println("\n1.-Ofertar");
                System.out.println("2.-Salir");
                opcion = sc.next();
                if(opcion.equals("1")){
                    System.out.println("\nIngrese su oferta: ");
                    cadena=v.getPlaca()+","+sc.next()+","+correo;
                    Util.Escribe(cadena, "Ofertas.txt");
                    i++;
                    System.out.println("\\\\Su oferta ha sido realizada//\n");

                }
                if(opcion.equals("2"))
                    i++;
                    
            } else if(i==lista.size()-1 && i!=0){
                System.out.println("\n1.-Anterior Vehículo");
                System.out.println("2.-Ofertar");
                System.out.println("3.-Salir");
                System.out.println("\n Ingrese su elección");
                opcion = sc.next();
                if(opcion.equals("1"))
                    i--;
                if(opcion.equals("2")){
                System.out.println("\nIngrese su oferta: ");
                cadena=v.getPlaca()+","+sc.next()+","+correo;
                Util.Escribe(cadena, "Ofertas.txt");
                i++;
                System.out.println("\\\\Su oferta ha sido realizada//\n");

                }
                if(opcion.equals("3"))
                    i=lista.size();
            }
            else if(lista.size()>1 && i<2){
                System.out.println("\n1.- Siguiente Vehículo"); 
                System.out.println("2.- Ofertar");
                System.out.println("3.- Salir");
                System.out.println("\nIngrese su eleccion: ");
                String opci = sc.next();
                if(opci.equals("1"))
                    i++;
                if(opci.equals("2")){
                    System.out.println("\nIngrese su oferta: ");
                    cadena=v.getPlaca()+","+sc.next()+","+correo;
                    Util.Escribe(cadena, "Ofertas.txt");
                    i++;
                    System.out.println("\\\\Su oferta ha sido realizada//\n");

            }
                if(opci.equals("3"))
                   i=lista.size();
            }   
             else if(i>=1 && i<lista.size()-1){
                System.out.println("\n1.- Siguiente Vehículo"); 
                System.out.println("2.- Ofertar"); 
                System.out.println("3.- Anterior Vehículo");
                System.out.println("4.- Salir");
                System.out.println("\nIngrese su eleccion: ");
                opcion = sc.next();
                if(opcion.equals("1"))
                    i++;
                if(opcion.equals("2")){
                    System.out.println("\nIngrese su oferta: ");
                    cadena=v.getPlaca()+","+sc.next()+","+correo;
                    Util.Escribe(cadena, "Ofertas.txt");
                    i++;
                    System.out.println("\\\\Su oferta ha sido realizada//\n");
                 }
                if(opcion.equals("3"))
                    i--;
                if(opcion.equals("4"))
                    i=lista.size();
            }
        }
    }
    
    public static ArrayList<Vehiculos> listaVehiculosFiltrado(ArrayList<Vehiculos> listaVehiculos) //Lista de un solo tipo de vehículo
    {
        Double[] recorrido;
        Integer[] año;
        Double[] precio;
        
        System.out.println("En caso de no especificar rango dar doble Enter\n");
        recorrido=Util.obtenerRangoD("Elija rango de recorrido (Ejemplo: Del 0 a 10000): ");
        año=Util.obtenerRangoI("Elija rango de años (Ejemplo: Del 2000 a 2020): ");
        precio=Util.obtenerRangoD("Elija un rango de precio (Ejemplo: De 0 a 1000): ");
        ArrayList<Vehiculos> vehiculosFiltro = new ArrayList<>();
        for (int i = 0; i < listaVehiculos.size(); i++) {
            Vehiculos vehi = listaVehiculos.get(i);
            String[] lee=Util.Lee("Ventas.txt", vehi.getPlaca(),1);
            Double preciov = Double.parseDouble(lee[2]);
            if((vehi.getRecorrido()>=recorrido[0] && vehi.getRecorrido()<=recorrido[1]) && (vehi.getAño()>=año[0] && vehi.getAño()<=año[1]) && (preciov>=precio[0] && preciov<=precio[1]))
                vehiculosFiltro.add(vehi);
            }
        return vehiculosFiltro;
    }
    
    
    @Override
    public String toString() {
        return (this.placa + "," + this.montoOfertado + "," + this.correo);
    }
}
