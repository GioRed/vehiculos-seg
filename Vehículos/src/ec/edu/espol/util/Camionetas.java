/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author GeovannyRojas
 */
public class Camionetas extends Vehiculos{
    private final String vidrio;
    
    public Camionetas(String placa, String motor, String color, String combustible, int recorrido, String marca, int año, String vidrio) {
        super(placa, motor, color, combustible, recorrido, marca, año);
        this.vidrio = vidrio;
    }
    
    @Override
    public String  mostrar()
    {
        return "Marca: "+this.marca+
        "\nAño: "+this.año+
        "\nRecorrido: "+this.recorrido+
        "\nVidrios: "+this.vidrio+
        "\nColor: "+this.color+
        "\nCombustible: "+this.combustible+
        "\nPlaca: "+this.placa+
        "\nPrecio: $"+Util.Lee("Ventas.txt", this.placa,1)[2];
    }
    
    public static ArrayList<Vehiculos> leerCamionetas()
    {   
        String archivo = System.getProperty("user.dir")+"\\Dts\\"+"Camionetas.txt";
        ArrayList<Vehiculos> Camiones = new ArrayList<>();
        try(Scanner sc = new Scanner(new File(archivo))){
            while(sc.hasNextLine()){
                String[] id = sc.nextLine().split(",");
                boolean prueba=Util.Lee("Ventas.txt",id[0], 1)[3].contains("false");
                if(prueba){
                    Vehiculos vehi = new Auto(id[0],id[1],id[2],id[3],Integer.parseInt(id[4]),id[5],Integer.parseInt(id[6]),id[7]);
                    Camiones.add(vehi);
                }
            }
        }
        catch(Exception e){
            FileReader fr = null;
        }
        return Camiones;
    }
    
    @Override
    public String toString()
    {
     
        return (this.placa+","+this.motor+","+this.color+","+this.combustible+","+this.recorrido+","+this.marca+","+this.año+","+this.vidrio);
    }
}

