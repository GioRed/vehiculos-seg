/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.common;
import ec.edu.espol.util.Util;
import ec.edu.espol.util.*;
import java.io.*;
import java.util.ArrayList;

/***********************************************
 *                                             *
 * @author VehiculoSEG                         *
 ***********************************************
 */
public class main {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException
    {
        String accionVende;
        String accionCompra;
        String vehiculo;
        Venta venta;
        Cuenta login,login2;
        boolean Inicio = true;
        Vendedor vendedor;
        Comprador comprador;
        
        
        
        String opcion = Util.desplegarMenu();
        while (Inicio){
            ArrayList<Vehiculos> listaVentaVehiculos = Venta.vincularTodosLosVehiculos();
            switch(opcion)
            {
                case "1":
                    accionVende = Vendedor.menuVendedor();
                    if(accionVende.equals("1"))
                        Persona.registro(0);
                    if(accionVende.equals("2")){
                        login=Cuenta.ValidaSesion();
                        if(login != null)
                            if(login.getTipocuenta().equals("0")){
                                System.out.println("--Acceso concedido--\n");
                                vehiculo = Vendedor.presentarVehiculos();
                                venta = Vendedor.registrarVehiculo(vehiculo,login.getUsuario());
                                Venta.registrarVenta(venta);
                                System.out.println("\\\\Registro exitoso//");
                             }
                        else
                                System.out.println("Acceso denegado");
                    }
                    if(accionVende.equals("3")){
                        login=Cuenta.ValidaSesion();
                        if(login != null)
                            if(login.getTipocuenta().equals("0")){
                                System.out.println("--Acceso concedido--\n");
                                vendedor = Vendedor.crearPersonaVendedor("personas.txt",login.getUsuario(),login);
                                vendedor.leerVenta(System.getProperty("user.dir")+"\\Dts\\"+"Ventas.txt");
                                vendedor.mostrarOfertas(listaVentaVehiculos);
                            }else
                                System.out.println("Acceso denegado");
                    }
                    if (accionVende.equals("4"))
                        opcion = Util.desplegarMenu();
                break;
                
                case "2":
                    accionCompra = Comprador.menuComprador();
                    if(accionCompra.equals("1"))
                        Persona.registro(1);
                    if(accionCompra.equals("2")){
                        login2=Cuenta.ValidaSesion();
                        if(login2 != null)
                            if(login2.getTipocuenta().equals("1")){
                                System.out.println("Acceso concedido");
                                comprador = Comprador.crearPersonaComprador(System.getProperty("user.dir")+"\\Dts\\"+"personas.txt",login2.getUsuario(),login2);
                                vehiculo = Comprador.mostrarVehiculos();
                                if(vehiculo.equals("")){
                                    if(listaVentaVehiculos.isEmpty())
                                        System.out.println("---No hay vehículos a la venta, pruebe más tarde---\n");
                                    else
                                    Oferta.vehiculosEnVenta(listaVentaVehiculos,comprador.getCuenta().getCorreo());
                                }
                                if(vehiculo.equalsIgnoreCase("Auto"))
                                {   
                                    ArrayList<Vehiculos> listaAuto = Auto.leerAutos();
                                    ArrayList<Vehiculos> vehiculosFiltrado;
                                    if(listaAuto.isEmpty())
                                        System.out.println("--- No hay autos a la venta, pruebe más tarde---\n");
                                    else{
                                        vehiculosFiltrado=Oferta.listaVehiculosFiltrado(listaAuto);
                                        Oferta.vehiculosEnVenta(vehiculosFiltrado, comprador.getCuenta().getCorreo());
                                    }
                                }
                                if(vehiculo.equalsIgnoreCase("Camion"))
                                {
                                    ArrayList<Vehiculos> listaCamion = Camion.leerCamiones();
                                    ArrayList<Vehiculos> vehiculosFiltrado;
                                    if(listaCamion.isEmpty())
                                        System.out.println("--- No hay camiones a la venta, pruebe más tarde---\n");
                                    else{
                                        vehiculosFiltrado=Oferta.listaVehiculosFiltrado(listaCamion);
                                        Oferta.vehiculosEnVenta(vehiculosFiltrado, comprador.getCuenta().getCorreo());
                                    }
                                }
                                if(vehiculo.equalsIgnoreCase("Camioneta"))
                                {
                                    ArrayList<Vehiculos> listaCamioneta = Camionetas.leerCamionetas();
                                    ArrayList<Vehiculos> vehiculosFiltrado;
                                    if(listaCamioneta.isEmpty())
                                        System.out.println("--- No hay camionetas a la venta, pruebe más tarde---\n");
                                    else{
                                        vehiculosFiltrado=Oferta.listaVehiculosFiltrado(listaCamioneta);
                                        Oferta.vehiculosEnVenta(vehiculosFiltrado, comprador.getCuenta().getCorreo());
                                    }
                                }
                                if(vehiculo.equalsIgnoreCase("Motocicleta"))
                                {
                                    ArrayList<Vehiculos> listaMoto= Motocicletas.leerMotos();
                                    ArrayList<Vehiculos> vehiculosFiltrado;
                                    if(listaMoto.isEmpty())
                                        System.out.println("--- No hay motos a la venta, pruebe más tarde---\n");
                                    else{
                                        vehiculosFiltrado = Oferta.listaVehiculosFiltrado(listaMoto);
                                        Oferta.vehiculosEnVenta(vehiculosFiltrado, comprador.getCuenta().getCorreo());
                                    }
                                }
                            }else
                                System.out.println("Acceso denegado");
                    }
                    if(accionCompra.equals("3"))
                        opcion = Util.desplegarMenu();
                    
                break;
                default:
                    Inicio = false;
                break;
            }
            
        }
    
    }
}