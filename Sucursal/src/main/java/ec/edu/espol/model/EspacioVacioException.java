/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

/**
 *
 * @author GeovannyRojas
 */
public class EspacioVacioException extends Exception {
    
    public EspacioVacioException()
    {
        super();
    }
    public EspacioVacioException(String msg)
    {
        super(msg);
    }
    
}
