/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author GeovannyRojas
 */
public class Cuenta implements Serializable {

    private String usuario;
    private String pass;
    private String correo;
    private String tipocuenta;
    private static final long serialVersionUID = 111100202020000L;

    public Cuenta(String usuario, String pass, String correo, String tipocuenta) {
        this.usuario = usuario;
        this.pass = pass;
        this.correo = correo;
        this.tipocuenta = tipocuenta;
    }

    public Cuenta() {
    }

    public String getTipocuenta() {
        return tipocuenta;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getPass() {
        return pass;
    }

    public String getCorreo() {
        return correo;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setTipocuenta(String tipocuenta) {
        this.tipocuenta = tipocuenta;
    }

    public static void registrar(ArrayList<Cuenta> cuenta) throws FileNotFoundException, IOException {
        try (FileOutputStream f = new FileOutputStream("Cuenta.bin");
                ObjectOutputStream out = new ObjectOutputStream(f)) {
            out.writeObject(cuenta);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public static boolean esCorreo(String correo) {
        int arroba = correo.indexOf('@');
        int com = correo.indexOf(".com");
        int es = correo.indexOf(".es");
        int ec = correo.indexOf(".ec");

        return (arroba != -1 && (com != -1 || es != -1 || ec != -1));
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        if (this == o) {
            return true;
        }

        Cuenta other = (Cuenta) o;

        return (Objects.equals(this.usuario, other.usuario));
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.usuario);
        return hash;
    }

    public static Cuenta validaSesion(String user, String pass) throws FindNotFoundException {   //Este se usa para un login comun 
        try {
            Cuenta credenciales = obtenerCuenta(user);
            if (credenciales.getPass().equals(pass)) {
                return credenciales;
            }
            throw new FindNotFoundException("Credenciales Incorrectas");
        } catch (Exception e) {
            throw new FindNotFoundException("No existen usuarios asociados a las credenciales indicadas");
        }

    }

    public static Cuenta validaSesion(String user) throws FindNotFoundException {                 //Este se usa para validar si un usuario existe para el registro
        try {
            Cuenta credenciales = obtenerCuenta(user);
            return credenciales;
        } catch (Exception e) {
            throw new FindNotFoundException("El usuario no existe");
        }
    }

    public static Cuenta obtenerCuenta(String user) throws FileNotFoundException, IOException, ClassNotFoundException, FindNotFoundException {
        for (Cuenta cuenta : listaCuentas()) {
            if (cuenta.getUsuario().equals(user)) {                           //Este metodo se usa para obtener una cuenta tomado como base un nombre de usuario
                return cuenta;
            }
        }
        throw new FindNotFoundException("Usuario no encontrado");
    }

    public static ArrayList<Cuenta> listaCuentas() {        //Este metodo obtiene un arraylist de todas las cuentas registradas
        ArrayList<Cuenta> lista = new ArrayList<>();
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("Cuenta.bin"));) {
            lista = (ArrayList<Cuenta>) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }return lista;

    }

    public static String verificaCorreo(String correo) throws EmailException {               //Este metodo verifica que el correo sea correcto y que no exista para ningun usuario
        if (esCorreo(correo)) {
            throw new EmailException("Email invalido.");
        }
        for (Cuenta cuenta : listaCuentas()) {
            if (cuenta.getCorreo().equals(correo)) {
                throw new EmailException("El Email ingresado pertenece a otro usuario");
            }
        }
        return correo;
    }

    public static boolean validaRegistro(String user, String correo) {
        ArrayList<Cuenta> listaC = Cuenta.listaCuentas();
        for (Cuenta cuenta : listaC) {
            if ((user.equals(cuenta.getUsuario()) || correo.equals(cuenta.getCorreo()))) {
                return false;
            }
        }
        return true;
    }
}
