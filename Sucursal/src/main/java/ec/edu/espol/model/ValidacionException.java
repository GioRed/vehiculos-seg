/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

/**
 *
 * @author GeovannyRojas
 */
public class ValidacionException extends Exception {

    public ValidacionException() {
        super();
    }

    public ValidacionException(String msg) {
        super(msg);
    }

    //Validar números en textos
    public static boolean verificarTexto(String texto) {
        try {
            int numero = Integer.parseInt(texto);
            return false;
        } catch (NumberFormatException e) {
            return true;
        }
    }

    //Validar texto en numeros
    public static boolean validarNumeros(String texto) {
        try {
            int numero = Integer.parseInt(texto);
            return numero >= 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean validarPrecio(String texto) {
        try {
            double numero = Double.parseDouble(texto);
            return numero >0;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    
    //Validar Cedula
    public static boolean validarCedula(String cedula) {
        try {
            if (cedula.length() == 10) {
                int digitotres = Integer.parseInt(cedula.substring(2, 3));
                if (digitotres < 6) {
                    int[] coefvalidar = {2, 1, 2, 1, 2, 1, 2, 1, 2}; // Coeficientes para la de validación cédula
                    int verificador = Integer.parseInt(cedula.substring(9, 10)); // El decimo digito es el que verifica cedula
                    int suma = 0;
                    for (int i = 0; i < (cedula.length() - 1); i++) {
                        int digito = Integer.parseInt(cedula.substring(i, i + 1)) * coefvalidar[i];
                        suma += ((digito % 10) + (digito / 10));
                    }
                    if (((suma % 10 == 0) && (suma % 10 == verificador)) || (10 - (suma % 10)) == verificador) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (NumberFormatException ex) {
            return false;
        }
    }

}
