/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author GeovannyRojas
 */
public class Vehiculo implements Serializable {

    private String tipoVehiculo,
            marca,
            combustible,
            placa,
            color,
            tipoVidrio;

    private int recorrido, año;
    private double precio;
    private String imagen;
    private boolean vendido = false;

    private Cuenta cuenta;
    private static final long serialVersionUID = 1334455662211L;

    //constructor para 4 ruedas
    public Vehiculo(String tipo, String marca, String combustible, String placa, String color, int recorrido, int año, double precio, String tipoVidrio, String archivoImg, Cuenta cuenta) {
        this.tipoVehiculo = tipo;
        this.marca = marca;
        this.combustible = combustible;
        this.placa = placa;
        this.color = color;
        this.recorrido = recorrido;
        this.año = año;
        this.precio = precio;
        this.tipoVidrio = tipoVidrio;
        this.imagen = archivoImg;
        this.cuenta = cuenta;
    }

    //constructor para 2 ruedas
    public Vehiculo(String tipo, String marca, String combustible, String placa, String color, int recorrido, int año, double precio, String archivoImg, Cuenta cuenta) {
        this.tipoVehiculo = tipo;
        this.marca = marca;
        this.combustible = combustible;
        this.placa = placa;
        this.color = color;
        this.recorrido = recorrido;
        this.año = año;
        this.precio = precio;
        this.imagen = archivoImg;
        this.cuenta = cuenta;
    }

    //Registrar Vehiculos
    public static void registrarVehi(ArrayList<Vehiculo> listaV) {
        try (FileOutputStream fOut = new FileOutputStream("Vehiculos.bin");
                ObjectOutputStream out = new ObjectOutputStream(fOut);) {
            out.writeObject(listaV);
        } catch (Exception e){
            System.out.println(e.getMessage());
            
        }
    }

    //Leer Registros
    public static ArrayList<Vehiculo> leerListaVehiculo() {
        ArrayList<Vehiculo> listaV = new ArrayList<>();
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("Vehiculos.bin"))) {
            listaV = (ArrayList<Vehiculo>) in.readObject();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return listaV;
    }

    //Getters and Setters
    public String getTipoVehiculo() {
        return tipoVehiculo;
    }

    public void setTipoVehiculo(String tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getCombustible() {
        return combustible;
    }

    public void setCombustible(String combustible) {
        this.combustible = combustible;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipoVidrio() {
        return tipoVidrio;
    }

    public void setTipoVidrio(String tipoVidrio) {
        this.tipoVidrio = tipoVidrio;
    }

    public int getRecorrido() {
        return recorrido;
    }

    public void setRecorrido(int recorrido) {
        this.recorrido = recorrido;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public boolean getEstado() {
        return vendido;
    }

    public void setEstado(boolean bln) {
        this.vendido = bln;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public static ArrayList<Vehiculo> vehiculosFiltrados(int recorridomax, int recorridomin, double preciomax, double preciomin, String tipo, int añomax, int añomin, Cuenta credenciales) {
        ArrayList<Vehiculo> vehiculosFiltrados = new ArrayList<>();
        for (Vehiculo vehiculo : Vehiculo.leerListaVehiculo()) {
            if (!vehiculo.getEstado() && !vehiculo.getCuenta().equals(credenciales) && (tipo.equals(vehiculo.getTipoVehiculo()) && (vehiculo.getAño() <= añomax && vehiculo.getAño() >= añomin && vehiculo.getRecorrido() <= recorridomax && vehiculo.getRecorrido() >= recorridomin && vehiculo.getPrecio() <= preciomax && vehiculo.getPrecio() >= preciomin))) {
                vehiculosFiltrados.add(vehiculo);
            }
        }
        return vehiculosFiltrados;
    }

    public static ArrayList<Vehiculo> vehiculosFiltrados(Cuenta credenciales) {
        ArrayList<Vehiculo> vehiculosFiltrados = new ArrayList<>();
        for (Vehiculo vehiculo : Vehiculo.leerListaVehiculo()) {
            if (!vehiculo.getEstado() && !vehiculo.getCuenta().equals(credenciales)) {
                vehiculosFiltrados.add(vehiculo);
            }
        }
        return vehiculosFiltrados;
    }

    public void registrarCompra() {
        ArrayList<Vehiculo> vehiculos = leerListaVehiculo();
        for (int i = 0; i < vehiculos.size(); i++) {
            if (vehiculos.get(i).equals(this)) {
                vehiculos.get(i).setEstado(true);
            }
        }
        registrarVehi(vehiculos);
        ArrayList<Oferta> ofertas = Oferta.listaOfertas();
        for (int i = 0; i < ofertas.size(); i++) {
            if (ofertas.get(i).getOfertado().getPlaca().equals(this.getPlaca())) {
                ofertas.get(i).getOfertado().setEstado(true);
            }
        }
        Oferta.registrarOfertas(ofertas);
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        if (this == o) {
            return true;
        }

        Vehiculo other = (Vehiculo) o;

        return (Objects.equals(this.placa, other.placa));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.placa);
        return hash;
    }

    @Override
    public String toString() {
        return this.getPlaca();
    }

    public String correo() {
        return "Tipo: " + tipoVehiculo
                + "\nMarca: " + marca
                + "\nPlaca: " + placa
                + "\nColor: " + color
                + "\nAño: " + año
                + "\nRecorrido: " + recorrido
                + "\nPrecio: " + precio;
    }
}
