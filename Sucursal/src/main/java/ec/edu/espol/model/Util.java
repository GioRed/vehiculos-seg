/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import java.util.Scanner;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author GeovannyRojas
 */
public class Util {
    
    private Util() {
    throw new IllegalStateException("Clase de Utilidad, no se puede instanciar");
  }

    public static String[] leerRemitente() {
        File archivo = new File("correo.properties");
        String[] id = new String[2];
        try (FileInputStream in = new FileInputStream(archivo)) {
            Properties prop = new Properties();
            prop.load(in);
            id[0] = prop.getProperty("remitente");
            id[1] = prop.getProperty("clave");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return id;
    }

    public static void enviarGmail(String destinatario, String asunto, String cuerpo) {
        String[] correo = leerRemitente();
        String remitente = correo[0], contraseña = correo[1];
        Properties props = System.getProperties();
        props.put("mail.smtp.host", "smtp.gmail.com");  //El servidor SMTP de Google
        props.put("mail.smtp.user", remitente);
        props.put("mail.smtp.clave", contraseña);    //La clave de la cuenta
        props.put("mail.smtp.auth", "true");    //Usar autenticación mediante usuario y clave
        props.put("mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", "587"); //El puerto SMTP seguro de Google

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(remitente));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));   //Se podrían añadir varios de la misma manera
            message.setSubject(asunto);
            message.setText(cuerpo);
            try (Transport transport = session.getTransport("smtp")) {
                transport.connect("smtp.gmail.com", remitente, contraseña);
                transport.sendMessage(message, message.getAllRecipients());
            }
        } catch (MessagingException me) {
            //Si se produce un error            
        }
    }

    // Codigo de encriptado, entendi la mitad (borrar este cmn JAJA)
    public static byte[] getSHA(String input) throws NoSuchAlgorithmException {
        // Static getInstance method is called with hashing SHA  
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        // digest() method called  
        // to calculate message digest of an input  
        // and return array of byte 
        return md.digest(input.getBytes(StandardCharsets.UTF_8));
    }

    public static String toHexString(byte[] hash) {
        // Convert byte array into signum representation  
        BigInteger number = new BigInteger(1, hash);

        // Convert message digest into hex value  
        StringBuilder hexString = new StringBuilder(number.toString(16));

        // Pad with leading zeros 
        while (hexString.length() < 32) {
            hexString.insert(0, '0');
        }

        return hexString.toString();

    }

    public static String codifica(String dato) {
        String hash = "";
        try {
            hash = toHexString(getSHA(dato));
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Exception thrown for incorrect algorithm: " + e);
        }
        return hash;
    }
}
