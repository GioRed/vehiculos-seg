/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
import javafx.scene.control.Alert;

/*Referencias :
    ref1:
    https://www.campusmvp.es/recursos/post/java-como-listar-filtrar-y-obtener-informacion-de-carpetas-y-archivos.aspx
    https://stackoverflow.com/questions/7347856/how-to-convert-a-string-into-an-arraylist
 */
/**
 *
 * @author GeovannyRojas
 */
public class Persona implements Serializable {

    private String nombres;
    private String apellidos;
    private String organizacion;
    private Cuenta cuenta;
    private String cedula;
    private static final long serialVersionUID = 122333444455555L;

    public Persona(String nombres, String apellidos, String organizacion, Cuenta cuenta, String cedula) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.organizacion = organizacion;
        this.cuenta = cuenta;
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public String getCedula() {
        return cedula;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    //Serialización
    public static void registro(ArrayList<Persona> persona) {
        try (FileOutputStream fOut = new FileOutputStream("Personas.bin");
                ObjectOutputStream out = new ObjectOutputStream(fOut);) {
            out.writeObject(persona);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //Leer serialización
    public static ArrayList<Persona> listaPersona() {
        ArrayList<Persona> listasP = new ArrayList<>();
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("Personas.bin"));) {
            listasP = (ArrayList<Persona>) in.readObject();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return listasP;
    }

    public static Persona obtenerPersona(String user) throws FindNotFoundException {
        Persona usuario = null;
        try {
            ArrayList<Persona> listaP = Persona.listaPersona();
            for (Persona p : listaP) {
                if (p.getCuenta().getUsuario().equals(user)) {
                    usuario = p;
                }
            }
            if (usuario == null) {
                throw new FindNotFoundException("No se encontro la cuenta vinculada al usuario");
            }
        } catch (FindNotFoundException e) {
            Alert a = new Alert(Alert.AlertType.WARNING, e.getMessage());
            a.show();
        }
        return usuario;
    }

    public boolean verificarPassword(String pass0, String pass1, String pass2) {
        try {
            if (pass0.equals(cuenta.getPass()) && pass1.equals(pass2)) {
                return true;
            } else if (!pass0.equals(cuenta.getPass())) {
                throw new ValidacionException("La contraseña anterior no coincide con la otorgada\nFavor verificar");
            } else if (!pass1.equals(pass2)) {
                throw new ValidacionException("La nueva contraseña no coincide con la confirmación de contraseña\n"
                        + "Favor reintentar");
            }

        } catch (ValidacionException e) {
            Alert a = new Alert(Alert.AlertType.INFORMATION, e.getMessage());
            a.show();
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        if (this == o) {
            return true;
        }

        Persona other = (Persona) o;

        return (Objects.equals(this.getCuenta(), other.getCuenta()));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.cuenta);
        return hash;
    }

}
