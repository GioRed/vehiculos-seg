/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author erisi
 */
public class Oferta implements Serializable {

    private Cuenta ofertante;
    private Vehiculo ofertado;
    private double precioOfertado;
    private static final long serialVersionUID = 123212321231213L;

    public Oferta(Cuenta ofertante, Vehiculo ofertado, double oferta) {
        this.ofertante = ofertante;
        this.ofertado = ofertado;
        this.precioOfertado = oferta;
    }

    public Cuenta getOfertante() {
        return ofertante;
    }

    public Vehiculo getOfertado() {
        return ofertado;
    }

    public double getPrecioOfertado() {
        return precioOfertado;
    }

    public void setOfertante(Cuenta ofertante) {
        this.ofertante = ofertante;
    }

    public void setOfertado(Vehiculo ofertado) {
        this.ofertado = ofertado;
    }

    public void setPrecioOfertado(double precioOfertado) {
        this.precioOfertado = precioOfertado;
    }

    public void registrarOferta() {
        ArrayList<Oferta> listaofertas = Oferta.listaOfertas();
        listaofertas.add(this);
        registrarOfertas(listaofertas);
    }

    public static void registrarOfertas(ArrayList<Oferta> listaofertas) {
        try (FileOutputStream fOut = new FileOutputStream("Ofertas.bin");
                ObjectOutputStream out = new ObjectOutputStream(fOut);) {
            out.writeObject(listaofertas);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static ArrayList<Oferta> listaOfertas() {
        ArrayList<Oferta> listasP = new ArrayList<>();
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("Ofertas.bin"));) {
            listasP = (ArrayList<Oferta>) in.readObject();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return listasP;
    }

    public static ArrayList<Vehiculo> listaOfertasFiltradas(Cuenta cVendedor) {
        ArrayList<Oferta> listaOfertas = listaOfertas();
        ArrayList<Vehiculo> ofertasFiltradas = new ArrayList<>();
        for (Oferta oferta : listaOfertas) {
            if (oferta.getOfertado().getCuenta().equals(cVendedor) && !oferta.ofertado.getEstado()) {
                ofertasFiltradas.add(oferta.getOfertado());
            }
        }
        return ofertasFiltradas;
    }

    public static ArrayList<Oferta> listaOfertas(String placa) {
        ArrayList<Oferta> listaOfertas = listaOfertas();
        ArrayList<Oferta> filtrada = new ArrayList<>();
        for (int i = 0; i < listaOfertas.size(); i++) {
            if (listaOfertas.get(i).getOfertado().getPlaca().equals(placa)) {
                filtrada.add(listaOfertas.get(i));
            }
        }
        return filtrada;
    }
}
