/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.*;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author USER
 */
public class VGeneralController implements Initializable {

    @FXML
    private TabPane tabpaneCompraVende;

    @FXML
    private Button btnofertarveh;

    @FXML
    private Button btncompsalir;

    @FXML
    private Button btnaceptarveh;

    @FXML
    private Button btnregistrarveh;

    @FXML
    private Button btnvendsalir;

    @FXML
    private Label lblusercomp;

    @FXML
    private Label lbluservend;

    @FXML
    private Tab idComprador;

    @FXML
    private Tab idVendedor;

    private Cuenta credenciales;
    private Persona usuario;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //fsdafdsafa
    }

    @FXML
    private void perfilUser(MouseEvent e) throws IOException, FindNotFoundException {

        FXMLLoader fxml = App.fxmLoad("VPerfilUsuario");
        App.llamarEscena(fxml, (Event) e);
        usuario = Persona.obtenerPersona(credenciales.getUsuario());
        VPerfilUsuarioController controlador = fxml.getController();
        controlador.llenarTexto(usuario.getNombres(), usuario.getApellidos(), usuario.getCedula(),
                usuario.getOrganizacion(), usuario.getCuenta().getUsuario(), usuario.getCuenta().getCorreo(), credenciales.getTipocuenta());
        controlador.getPersona(usuario);
    }

    @FXML
    private void verOfertas(MouseEvent event) throws IOException, FindNotFoundException //Vendedor
    {
        FXMLLoader fxml = App.fxmLoad("VAceptarOferta");
        App.llamarEscena(fxml, (Event) event);
        VAceptarOferta controlador = fxml.getController();
        controlador.setUser(Persona.obtenerPersona(credenciales.getUsuario()));
        controlador.llenarPlaca();

    }

    @FXML
    private void registrarVeh(MouseEvent event) throws IOException, FindNotFoundException { //Vendedor
        FXMLLoader fxml = App.fxmLoad("VRegistroVeh");
        App.llamarEscena(fxml, (Event) event);
        VRegistroVehController controlador = fxml.getController();
        controlador.setCuenta(credenciales);
    }

    @FXML
    private void BuscarVehiculo(MouseEvent event) throws IOException { //Comprador
        FXMLLoader fxml = App.fxmLoad("VBuscarvehiculo");
        App.llamarEscena(fxml, (Event) event);
        VBuscarvehiculo controlador = fxml.getController();
        controlador.setCuenta(credenciales);
        controlador.llenarCombo();
        controlador.llenarTablaInicio();
    }

    @FXML
    private void Salir(MouseEvent event) throws IOException {
        Alert salida = new Alert(AlertType.CONFIRMATION);
        salida.setTitle("Confirmacion de Salida");
        salida.setContentText("Esta seguro que desea salir?");

        Optional<ButtonType> result = salida.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
                App.llamarEscena("VLogin", (Event) event);
        }
    }

    public Cuenta getCuenta( ) {
        return credenciales;
    }

    public void setear() {
        switch (credenciales.getTipocuenta()) {
            case "Comprador":
                tabpaneCompraVende.getTabs().remove(1);
                break;
            case "Vendedor":
                tabpaneCompraVende.getTabs().remove(0);
                break;
            default:
                break;
        }
    }

    public void setCuenta(Cuenta credenciales) {
        this.credenciales = credenciales;
    }

}
