/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Cuenta;
import ec.edu.espol.model.EspacioVacioException;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.ValidacionException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author
 */
public class VRegPersonaController implements Initializable {

    @FXML
    private TextField txtnombres;
    @FXML
    private TextField txtapellidos;
    @FXML
    private TextField txtcedula;
    @FXML
    private TextField txtorg;
    @FXML
    private Button btncancelar;
    @FXML
    private Button btnregistrar;
    @FXML
    private Label lblerrorcorreo;
    @FXML
    private Label lblerrorced;

    private Cuenta credenciales;

    /**
     * Initializes the controller class.
     */

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Este espacio es para inicializar la aplicacion
    }

    @FXML
    private void cancelarOperacion(MouseEvent event) throws IOException {
        App.llamarEscena("VLogin", (Event) event);
    }

    @FXML
    private void registrarPersona(MouseEvent event) throws IOException {
        ArrayList<Cuenta> cuentas = Cuenta.listaCuentas();
        ArrayList<Persona> listaPersonas = Persona.listaPersona();
        cuentas.add(credenciales);
        try {

            if (txtnombres.getText().equals("") || txtapellidos.getText().equals("")
                    || txtcedula.getText().equals("") || txtorg.getText().equals("")) {
                throw new EspacioVacioException("Es necesario llenas los espacios vacios");
            } else if (!ValidacionException.verificarTexto(txtnombres.getText())
                    || !ValidacionException.verificarTexto(txtapellidos.getText())
                    || !ValidacionException.validarCedula(txtcedula.getText())) {
                throw new ValidacionException("Por favor ingrese datos válidos");
            }

            Persona user = new Persona(txtnombres.getText(), txtapellidos.getText(), txtorg.getText(),
                     credenciales, txtcedula.getText());
            listaPersonas.add(user);

            Cuenta.registrar(cuentas);
            Persona.registro(listaPersonas);
            Alert a = new Alert(Alert.AlertType.INFORMATION, "Se ha registrado correctamente\n"
                    + "Favor Inicie Sesión");
            a.setTitle("**Registro exitoso**");
            a.showAndWait();
            App.llamarEscena("VLogin", (Event) event);

        } catch (EspacioVacioException | ValidacionException | IOException e) {
            Alert alerta = new Alert(Alert.AlertType.INFORMATION, e.getMessage());
            alerta.show();
        }
    }

    public void setCuenta(Cuenta credenciales) {
        this.credenciales = credenciales;
    }

}
