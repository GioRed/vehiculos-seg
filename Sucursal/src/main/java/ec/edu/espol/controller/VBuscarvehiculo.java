/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 /*
    REFERENCIAS:
    https://www.youtube.com/watch?v=rDiLjbOPyN4

 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Cuenta;
import ec.edu.espol.model.EspacioVacioException;
import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.ValidacionException;
import ec.edu.espol.model.Vehiculo;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author USER
 */
public class VBuscarvehiculo implements Initializable {

    @FXML
    private TableView<Vehiculo> tblvopciones;

    @FXML
    private Button btnbuscar;

    @FXML
    private ComboBox cmbtipveh;

    @FXML
    private TextField txtminP;

    @FXML
    private Button btnofertar;

    @FXML
    private TextField txtmaxP;

    @FXML
    private TextField txtmonto;

    @FXML
    private TextField txtminR;

    @FXML
    private TextField txtmaxR;

    @FXML
    private TextField txtminA;

    @FXML
    private TextField txtmaxA;

    @FXML
    private ImageView imagenVehi;

    private Cuenta credenciales;

    public Cuenta getCredenciales() {
        return credenciales;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (Vehiculo.leerListaVehiculo().isEmpty()) {
            Alert a = new Alert(Alert.AlertType.INFORMATION, "No hay registros de Vehiculos");
            a.show();
        } else {
            this.llenarTabla(Vehiculo.leerListaVehiculo());
        }
    }

    @FXML
    private void regresar(MouseEvent event) throws IOException {
        FXMLLoader fxml = App.fxmLoad("VUGeneral");
        App.llamarEscena(fxml, (Event) event);
        VGeneralController controller = fxml.getController();
        controller.setCuenta(this.getCredenciales());
        controller.setear();
    }

    @FXML
    private void seleccionado(MouseEvent event) {
        txtmonto.setEditable(true);
        btnofertar.setDisable(false);
        tblvopciones.setOnMouseClicked((Event t) -> {
            Vehiculo vehiculo = tblvopciones.getSelectionModel().getSelectedItem();
            Image lala = new Image(vehiculo.getImagen());
            imagenVehi.setImage(lala);
        });
    }

    @FXML
    private void filtrar(MouseEvent event) {

        // LO DEL ESPACIO VACIO NO LO SE SI SEA NECESARIO, PERO SE ENCUENTRA LA VALIDACION DE NUMEROS
        try {
            if (txtmaxR.getText().equals("") || txtminR.getText().equals("")
                    || txtmaxA.getText().equals("") || txtminA.getText().equals("")
                    || txtmaxP.getText().equals("") || txtminP.getText().equals("")
                    || cmbtipveh.getValue().toString().equals("")) {
                throw new EspacioVacioException("Es necesario llenar los espacios vacios. Ingrese de nuevo");
            } else if (!ValidacionException.validarNumeros(txtmaxR.getText())
                    || !ValidacionException.validarNumeros(txtminR.getText())
                    || !ValidacionException.validarNumeros(txtmaxA.getText())
                    || !ValidacionException.validarNumeros(txtminA.getText())
                    || !ValidacionException.validarPrecio(txtmaxP.getText())
                    || !ValidacionException.validarPrecio(txtminP.getText())) {
                throw new ValidacionException("Por favor ingrese datos válidos");
            }

            int rmax = Integer.parseInt(txtmaxR.getText());
            int rmin = Integer.parseInt(txtminR.getText());
            int amax = Integer.parseInt(txtmaxA.getText());
            int amin = Integer.parseInt(txtminA.getText());
            
            double pmax = Double.parseDouble(txtmaxP.getText());
            double pmin = Double.parseDouble(txtminP.getText());
            tblvopciones.getColumns().clear();
            ArrayList<Vehiculo> listavehiculos = Vehiculo.vehiculosFiltrados(rmax, rmin, pmax, pmin, cmbtipveh.getValue().toString(), amax, amin, this.getCredenciales());
            this.llenarTabla(listavehiculos);
        } catch (EspacioVacioException | ValidacionException e) {
            Alert alerta = new Alert(Alert.AlertType.INFORMATION, e.getMessage());
            alerta.show();
        }
    }

    @FXML
    public void ofertar(MouseEvent event) {
        try {
            if (txtmonto.getText().equals("")) {
                throw new EspacioVacioException("Es necesario llenar los espacios vacios. Ingrese de nuevo");
            } else if (!ValidacionException.validarPrecio(txtmonto.getText())) {
                throw new ValidacionException("Por favor ingrese datos válidos");
            }

            Oferta oferta = new Oferta(credenciales, tblvopciones.getSelectionModel().getSelectedItem(), Double.parseDouble(txtmonto.getText()));

            oferta.registrarOferta();
            txtmonto.clear();
            Alert venta = new Alert(Alert.AlertType.INFORMATION, "Ha realizado la oferta del vehiculo con exito");
            venta.setTitle("Ofertar vehiculo");
            venta.showAndWait();
            FXMLLoader fxml = App.fxmLoad("VUGeneral");
            App.llamarEscena(fxml, (Event) event);
            VGeneralController controlador = fxml.getController();
            controlador.setCuenta(getCredenciales());
            controlador.setear();

        } catch (EspacioVacioException | ValidacionException | IOException e) {
            Alert alerta = new Alert(Alert.AlertType.INFORMATION, e.getMessage());
            alerta.show();
        }
    }

    public void setCuenta(Cuenta credenciales) {
        this.credenciales = credenciales;
    }

    public void llenarCombo() {
        ArrayList<String> tiposVehiculo = new ArrayList<>();
        tiposVehiculo.add("Auto");
        tiposVehiculo.add("Camion");
        tiposVehiculo.add("Camionetas");
        tiposVehiculo.add("Motocicletas");
        cmbtipveh.setItems(FXCollections.observableArrayList(tiposVehiculo));
    }

    private void llenarTabla(ArrayList<Vehiculo> listavehiculos) {
        ObservableList<Vehiculo> vehic = FXCollections.observableArrayList(listavehiculos);
        tblvopciones.setItems(vehic);

        TableColumn<Vehiculo, String> colTipo = new TableColumn<>("Tipo de vehiculo");
        colTipo.setCellValueFactory(new PropertyValueFactory<Vehiculo, String>("tipoVehiculo"));
        colTipo.setMinWidth(130);

        TableColumn<Vehiculo, String> colPlaca = new TableColumn<>("Placa");
        colPlaca.setCellValueFactory(new PropertyValueFactory<Vehiculo, String>("placa"));
        colPlaca.setMinWidth(80);

        TableColumn<Vehiculo, String> colMarca = new TableColumn<>("Marca");
        colMarca.setCellValueFactory(new PropertyValueFactory<Vehiculo, String>("marca"));
        colMarca.setMinWidth(100);

        TableColumn<Vehiculo, Integer> colRecorrido = new TableColumn<>("Recorrido");
        colRecorrido.setCellValueFactory(new PropertyValueFactory<Vehiculo, Integer>("recorrido"));
        colRecorrido.setMinWidth(100);

        TableColumn<Vehiculo, Integer> colAnio = new TableColumn<>("Año");
        colAnio.setCellValueFactory(new PropertyValueFactory<Vehiculo, Integer>("año"));
        colAnio.setMinWidth(80);

        TableColumn<Vehiculo, String> colColor = new TableColumn<>("Color");
        colColor.setCellValueFactory(new PropertyValueFactory<Vehiculo, String>("color"));
        colColor.setMinWidth(tblvopciones.getMaxWidth() / 4);

        TableColumn<Vehiculo, Double> colPrecio = new TableColumn<>("Precio");
        colPrecio.setCellValueFactory(new PropertyValueFactory<Vehiculo, Double>("precio"));
        colPrecio.setMinWidth(100);

        tblvopciones.getColumns().addAll(colTipo, colPlaca, colMarca, colPrecio, colRecorrido, colAnio, colColor);
    }

    public void llenarTablaInicio() {
        tblvopciones.getColumns().clear();
        this.llenarTabla(Vehiculo.vehiculosFiltrados(this.getCredenciales()));
    }
}
