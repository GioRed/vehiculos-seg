/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 /*
REFERENCIAS:
https://byspel.com/copiar-archivos-en-java-usando-jfilechooser/
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Cuenta;
import ec.edu.espol.model.EspacioVacioException;
import ec.edu.espol.model.FindNotFoundException;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.ValidacionException;
import ec.edu.espol.model.Vehiculo;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

/**
 * FXML Controller class
 *
 * @author USER
 */
public class VRegistroVehController implements Initializable {

    @FXML
    private GridPane gpaneregvehiculo;
    @FXML
    private GridPane gpaneimagen;
    @FXML
    private Button btnimagenveh;
    @FXML
    private ImageView imgvehiculoreg;
    @FXML
    private TextField txtplaca; 
    @FXML
    private ComboBox cmbtipoveh;
    @FXML
    private ComboBox cbmcombustible;
    @FXML
    private Button btnvolver;
    @FXML
    private Button btnregistrar;
    @FXML
    private ComboBox cmbtipovidrio;
    @FXML
    private Label lbltipovidrio;
    @FXML
    private TextField txtyear; 
    @FXML
    private TextField txtcolor; 
    @FXML
    private TextField txtprecio; 
    @FXML
    private TextField txtrecorrido; 
    @FXML
    private TextField txtmarca;
    
    private Cuenta cuenta;

    private Persona usuario;

    /**
     * Initializes the controller class.
     */
    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta c) {
        this.cuenta = c;
        try {
            this.usuario = Persona.obtenerPersona(c.getUsuario());
        } catch (FindNotFoundException ex) {
            // 
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mostrarVidrios(cmbtipoveh);
        llenarComboBox();
    }

    @FXML
    private void agregarImagen(MouseEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Imagenes de vehiculos");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );

        File imgFile = fileChooser.showOpenDialog(null);

        if (imgFile != null) {
            String destino = System.getProperty("user.dir")+"/target"+ imgFile.getName();
            Path dest = Paths.get(destino);
            String origen = imgFile.getPath();
            Path orig = Paths.get(origen);
            Files.copy(orig, dest, REPLACE_EXISTING);

            Image image = new Image("file:" + imgFile.getAbsolutePath());
            imgvehiculoreg.setImage(image);
        }
    }

    @FXML
    private void regresar(MouseEvent event) throws IOException {
        FXMLLoader fxml = App.fxmLoad("VUGeneral");
        App.llamarEscena(fxml, (Event) event);
        VGeneralController controlador = fxml.getController();
        controlador.setCuenta(usuario.getCuenta());
        controlador.setear();

    }

    @FXML
    private void registrar(MouseEvent event) throws FindNotFoundException {
        Vehiculo v;
        try {

            if (txtmarca.getText().equals("") || txtplaca.getText().equals("")
                    || txtcolor.getText().equals("") || cmbtipoveh.getValue().toString().equals("")
                    || cbmcombustible.getValue().toString().equals("") || imgvehiculoreg.toString().equals("")) {
                throw new EspacioVacioException("Es necesario llenar los espacios vacios. Ingrese de nuevo");
            } else if (!ValidacionException.verificarTexto(txtmarca.getText())
                    || !ValidacionException.verificarTexto(txtcolor.getText())
                    || !ValidacionException.validarNumeros(txtrecorrido.getText())
                    || !ValidacionException.validarNumeros(txtyear.getText())
                    || !ValidacionException.validarPrecio(txtprecio.getText())) {
                throw new ValidacionException("Por favor ingrese datos válidos");
            }
            if (cmbtipoveh.getValue().equals("Motocicleta")) {
                v = new Vehiculo(cmbtipoveh.getValue().toString(),
                        txtmarca.getText(), cbmcombustible.getValue().toString(),
                        txtplaca.getText(), txtcolor.getText(), Integer.parseInt(txtrecorrido.getText()),
                        Integer.parseInt(txtyear.getText()), Double.parseDouble(txtprecio.getText()), imgvehiculoreg.getImage().getUrl(),
                        cuenta);
            } else {
                v = new Vehiculo(cmbtipoveh.getValue().toString(),
                        txtmarca.getText(), cbmcombustible.getValue().toString(),
                        txtplaca.getText(), txtcolor.getText(), Integer.parseInt(txtrecorrido.getText()),
                        Integer.parseInt(txtyear.getText()), Double.parseDouble(txtprecio.getText()), cmbtipovidrio.getValue().toString(),
                        imgvehiculoreg.getImage().getUrl(), cuenta);
            }
            ArrayList<Vehiculo> vehiculo = Vehiculo.leerListaVehiculo();
            vehiculo.add(v);

            Vehiculo.registrarVehi(vehiculo);
            Alert a = new Alert(Alert.AlertType.INFORMATION, "Vehiculo registrado correctamente");
            a.setTitle("Registro exitoso");
            a.showAndWait();
            FXMLLoader fxml = App.fxmLoad("VUGeneral");
            App.llamarEscena(fxml, (Event) event);
            VGeneralController controlador = fxml.getController();
            controlador.setCuenta(cuenta);
            controlador.setear();

        } catch (EspacioVacioException | ValidacionException | IOException e) {
            Alert alerta = new Alert(Alert.AlertType.INFORMATION, e.getMessage());
            alerta.show();
        } catch (NullPointerException nul) {
            Alert alerta = new Alert(Alert.AlertType.INFORMATION, "Por favor seleccione la imagen del vehiculo.");
            alerta.show();
        }
    }

    private void mostrarVidrios(ComboBox cbx) {
        cbx.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                ComboBox combo = (ComboBox) t.getSource();
                if (combo.getValue().toString().equals("Motocicletas")) {
                    lbltipovidrio.setVisible(false);
                    cmbtipovidrio.setVisible(false);
                    cmbtipovidrio.setValue("");
                } else {
                    lbltipovidrio.setVisible(true);
                    cmbtipovidrio.setVisible(true);
                }
            }
        });
    }

    private void llenarComboBox() {
        ArrayList<String> tiposVehiculo = new ArrayList<>();
        tiposVehiculo.add("Auto");
        tiposVehiculo.add("Camion");
        tiposVehiculo.add("Camionetas");
        tiposVehiculo.add("Motocicletas");
        cmbtipoveh.setItems(FXCollections.observableArrayList(tiposVehiculo));

        ArrayList<String> combustibles = new ArrayList<>();
        combustibles.add("Gasolina");
        combustibles.add("Diesel");
        combustibles.add("Otro");
        cbmcombustible.setItems(FXCollections.observableArrayList(combustibles));

        ArrayList<String> vidrio = new ArrayList<>();
        vidrio.add("Electricos");
        vidrio.add("Mécanico");
        cmbtipovidrio.setItems(FXCollections.observableArrayList(vidrio));
    }

}
