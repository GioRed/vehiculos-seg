/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Util;
import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vehiculo;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;

/**
 *
 * @author USER
 */
public class VAceptarOferta implements Initializable {

    @FXML
    private TableColumn<Vehiculo, String> colOfertas;

    @FXML
    private Button btnsalir;

    @FXML
    private Button btnaceptaroferta;

    @FXML
    private ComboBox cmbvehioferta;

    @FXML
    private TableView tbvcontenido;

    @FXML
    private Text txtcorreo;

    @FXML
    private Text txtinfoComprador;

    private Persona usuario;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtcorreo.setText("");
        mostrarInfo(tbvcontenido);
    }

    @FXML
    private void salir(MouseEvent event) throws IOException {
        FXMLLoader fxml = App.fxmLoad("VUGeneral");
        App.llamarEscena(fxml, (Event) event);
        VGeneralController controller = fxml.getController();
        controller.setCuenta(usuario.getCuenta());
        controller.setear();
    }

    @FXML
    private void aceptar(MouseEvent event) {
        try {
            Oferta of = (Oferta) tbvcontenido.getSelectionModel().getSelectedItem();
            of.getOfertado().registrarCompra();
            Util.enviarGmail(of.getOfertante().getCorreo(), "Oferta de Vehiculo", "Su oferta sobre este vehículo\n\n" + of.getOfertado().correo() + "\n\n Ha sido aceptada\nFavor comunicarse con: " + usuario.getCuenta().getCorreo() + "\n Gracias por preferirnos.");
            this.llenarPlaca();
            txtcorreo.setText("Correo Enviado");
            txtinfoComprador.setText("Por favor, elija una placa");
            colOfertas.getColumns().clear();
        } catch (Exception e) {
            Alert var = new Alert(Alert.AlertType.INFORMATION, "Seleccione una Oferta por favor.");
            var.show();
        }
    }

    private void mostrarInfo(TableView tv) {

        tv.setOnMouseClicked((Event t) -> {
            txtinfoComprador.setText("");
            Oferta of = (Oferta) tv.getSelectionModel().getSelectedItem();
            txtinfoComprador.setText("Usuario que realizo la oferta: " + of.getOfertante().getUsuario());
        });

    }

    @FXML
    private void filtrar(ActionEvent event) {
        try {
            ArrayList<Oferta> ofertas = Oferta.listaOfertas(cmbvehioferta.getValue().toString());
            colOfertas.getColumns().clear();
            if (!ofertas.isEmpty()) {
                llenarGrid(ofertas);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void llenarPlaca() {
        txtcorreo.setText("");
        cmbvehioferta.getItems().clear();
        ArrayList<Vehiculo> listaV = Oferta.listaOfertasFiltradas(usuario.getCuenta());
        ArrayList<String> placas = new ArrayList<>();
        for (Vehiculo v : listaV) {
            if ((!v.getEstado() && v.getCuenta().equals(getUser().getCuenta())) && !placas.contains(v.getPlaca())) {
                placas.add(v.getPlaca());
            }
        }
        if (!placas.isEmpty()) {
            cmbvehioferta.setItems(FXCollections.observableArrayList(placas));
        }
    }

    public void llenarGrid(ArrayList<Oferta> ofertas) {
        ObservableList<Oferta> vehic = FXCollections.observableArrayList(ofertas);
        tbvcontenido.setItems(vehic);

        TableColumn<Vehiculo, String> colPlaca = new TableColumn<>("Placa");
        colPlaca.setCellValueFactory(new PropertyValueFactory<Vehiculo, String>("ofertado"));
        colPlaca.setMinWidth(175);

        TableColumn<Vehiculo, Double> colMonto = new TableColumn<>("Monto");
        colMonto.setCellValueFactory(new PropertyValueFactory<Vehiculo, Double>("precioOfertado"));
        colMonto.setMinWidth(175);

        colOfertas.getColumns().addAll(colPlaca, colMonto);
        colOfertas.setPrefWidth(tbvcontenido.getPrefWidth());
    }

    public void setUser(Persona p) {
        this.usuario = p;
    }

    public Persona getUser() {
        return usuario;
    }
}
