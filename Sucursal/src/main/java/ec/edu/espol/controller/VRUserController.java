/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Cuenta;
import ec.edu.espol.model.EmailException;
import ec.edu.espol.model.EspacioVacioException;
import ec.edu.espol.model.FindNotFoundException;
import ec.edu.espol.model.ValidacionException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
/**
 * Referencias:
 *      https://es.stackoverflow.com/questions/33922/cambio-de-frame-en-javafx
 * 
 * FXML Controller class
 *
 * @author 
 */
public class VRUserController implements Initializable {


    @FXML
    private TextField txtUser;
    @FXML
    private PasswordField txtPass1;
    @FXML
    private PasswordField txtPass2;
    @FXML
    private Label lblAduser;
    @FXML
    private Label lblAdcontraseña;
    @FXML
    private Button btnComprobar;
    @FXML
    private Button btnCancelar;
    @FXML
    private ComboBox cmbxUsertype;
     @FXML
    private TextField txtCorreo;
     
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ArrayList<String> tiposUsuario = new ArrayList<>();
        tiposUsuario.add("Comprador");
        tiposUsuario.add("Vendedor");
        tiposUsuario.add("Compra y Venta");
        cmbxUsertype.setItems(FXCollections.observableArrayList(tiposUsuario));

    }    
    
    @FXML
    private void ValidaSesion(MouseEvent event) throws FindNotFoundException, IOException, EspacioVacioException, EmailException, ValidacionException 
    {
        try{
            //Validaciones de campos
            if(!(txtPass1.getText().equals(txtPass2.getText())))
               throw new FindNotFoundException("Las contraseñas no coinciden\nReintentar");
            
            else if(txtPass1.getText().equals("") || txtPass2.getText().equals("") || txtUser.getText().equals("") || txtCorreo.getText().equals("")|| cmbxUsertype.getValue().toString().equals(""))
               throw new EspacioVacioException("Es necesario llenar los espacios en blanco");
            
            else if(!Cuenta.esCorreo(txtCorreo.getText()))
                throw new EmailException("Ingrese un correo válido");
            //Validacion en Usuario y Correo
            if(Cuenta.validaRegistro(txtUser.getText(), txtCorreo.getText())){
                Cuenta credenciales = new Cuenta(txtUser.getText(),txtPass1.getText(),txtCorreo.getText(),cmbxUsertype.getValue().toString());
                FXMLLoader fxml = App.fxmLoad("VRegPersona");
                App.llamarEscena(fxml,(Event)event);
                VRegPersonaController controller = fxml.getController();
                controller.setCuenta(credenciales);
            }else
                throw new ValidacionException("No se ha podido registrar\nUsuario o Correo ya están ingresados\nIntente nuevamente");
         
        }catch(FindNotFoundException | EspacioVacioException | EmailException | ValidacionException e)
        {
            Alert a = new Alert(Alert.AlertType.INFORMATION,e.getMessage());
            a.show();
        }
    }

    @FXML
    private void stop(MouseEvent event) throws IOException {
        App.llamarEscena("VLogin",(Event) event);
    }

}
