/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import ec.edu.espol.model.*;
import javafx.scene.control.Alert;
/**
 * FXML Controller class
 *
 * @author 
 */
public class VLoginController implements Initializable {

    @FXML
    private AnchorPane AnchPaneroot;
    @FXML
    private TextField txtuser;
    @FXML
    private PasswordField txtpass;
    @FXML
    private Button btningreso;
    @FXML
    private Label txtregVendedor;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Este metodo esta sobreescrito y declarado porque son las acciones que toma al iniciar esta ventana.
    }    

    @FXML
    private void Validar(MouseEvent event) throws IOException {
        try {
            Cuenta credenciales = Cuenta.validaSesion(txtuser.getText(), txtpass.getText()); //Nota mental. Implementar la codificacion cuando se haga el registro
            FXMLLoader fxml = App.fxmLoad("VUGeneral");
            App.llamarEscena(fxml,(Event) event);
            VGeneralController controller = fxml.getController();
            controller.setCuenta(credenciales);
            controller.setear();            
        } catch (FindNotFoundException ex) {
            Alert var = new Alert (Alert.AlertType.INFORMATION,ex.getMessage());
            var.show();
        } 
    }

    @FXML
    private void Registrar(MouseEvent event) throws IOException {
        App.llamarEscena("VRUser",(Event) event);
    }
}
