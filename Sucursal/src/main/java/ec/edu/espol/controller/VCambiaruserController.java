/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Cuenta;
import ec.edu.espol.model.EspacioVacioException;
import ec.edu.espol.model.Persona;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author USER
 */
public class VCambiaruserController implements Initializable {

    @FXML
    private ComboBox cmbtipouserc;

    @FXML
    private Button btnvolveruser;

    @FXML
    private Button btnsaveuser;

    private Persona usuario;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ArrayList<String> tipo = new ArrayList<>();
        tipo.add("Comprador");
        tipo.add("Vendedor");
        tipo.add("Compra y Venta");
        cmbtipouserc.setItems(FXCollections.observableArrayList(tipo));
    }

    @FXML
    private void guardar(MouseEvent e) {
        try {
            if (cmbtipouserc.getValue().toString().equals("")) {
                throw new EspacioVacioException("Es necesario llenar los espacios vacios. Ingrese de nuevo");
            }

            ArrayList<Cuenta> listaC = Cuenta.listaCuentas();
            ArrayList<Persona> listaP = Persona.listaPersona();
            //cambio tipo usuario
            for (int i = 0; i < listaC.size(); i++) {
                Cuenta c = listaC.get(i);
                if (c.getUsuario().equals(usuario.getCuenta().getUsuario())) {
                    listaC.get(i).setTipocuenta(cmbtipouserc.getValue().toString());
                    listaP.get(i).getCuenta().setTipocuenta(cmbtipouserc.getValue().toString());
                }
            }

            Persona.registro(listaP);
            Cuenta.registrar(listaC);
            Alert a = new Alert(Alert.AlertType.INFORMATION, "Debe volver a iniciar sesión");
            a.showAndWait();
            FXMLLoader fxml = App.fxmLoad("VLogin");
            App.llamarEscena(fxml, (Event) e);

        } catch (EspacioVacioException | IOException esp) {
            Alert alerta = new Alert(Alert.AlertType.INFORMATION, esp.getMessage());
            alerta.show();
        }
    }

    @FXML
    private void salir(MouseEvent event) throws IOException {
        Alert salida = new Alert(Alert.AlertType.CONFIRMATION);
        salida.setTitle("Confirmacion de Salida");
        salida.setContentText("Esta seguro que desea salir?");

        Optional<ButtonType> result = salida.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
                FXMLLoader fxml = App.fxmLoad("VPerfilUsuario");
                App.llamarEscena(fxml, (Event) event);
                VPerfilUsuarioController controlador = fxml.getController();
                controlador.getPersona(usuario);
                controlador.llenarTexto(usuario.getNombres(), usuario.getApellidos(), usuario.getCedula(),
                        usuario.getOrganizacion(), usuario.getCuenta().getUsuario(), usuario.getCuenta().getCorreo(), usuario.getCuenta().getTipocuenta());
                controlador.getPersona(usuario);
        }
    }

    public void setUser(Persona user) {
        this.usuario = user;
    }
}
