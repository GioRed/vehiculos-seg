/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Cuenta;
import ec.edu.espol.model.EspacioVacioException;
import ec.edu.espol.model.Persona;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author USER
 */
public class VCambiarpasswordController implements Initializable {

    @FXML
    private PasswordField txtnewpass;

    @FXML
    private PasswordField txtconfpass;

    @FXML
    private PasswordField txtpass;
    @FXML
    private Button btnvolverpass;

    @FXML
    private Button btnsavepassw;

    private Persona usuario;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //No hay ninguna función
    }

    @FXML
    private void volver(MouseEvent e) throws IOException {
        Alert salida = new Alert(Alert.AlertType.CONFIRMATION);
        salida.setTitle("Confirmacion de Salida");
        salida.setContentText("Esta seguro que desea salir?");

        Optional<ButtonType> result = salida.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            FXMLLoader fxml = App.fxmLoad("VPerfilUsuario");
            App.llamarEscena(fxml, (Event) e);
            VPerfilUsuarioController controlador = fxml.getController();
            controlador.getPersona(usuario);
            controlador.llenarTexto(usuario.getNombres(), usuario.getApellidos(), usuario.getCedula(),
                    usuario.getOrganizacion(), usuario.getCuenta().getUsuario(), usuario.getCuenta().getCorreo(), usuario.getCuenta().getTipocuenta());
            controlador.getPersona(usuario);
        }
    }

    @FXML
    private void guardar(MouseEvent e) throws IOException {
        try {
            if (txtpass.getText().equals("") || txtnewpass.getText().equals("") || txtconfpass.getText().equals("")) {
                throw new EspacioVacioException("Es necesario llenar los espacios vacios. Ingrese de nuevo");
            }
            ArrayList<Cuenta> listaC = Cuenta.listaCuentas();
            if (usuario.verificarPassword(txtpass.getText(), txtnewpass.getText(), txtconfpass.getText())) {
                //cambio Password
                for (int i = 0; i <= listaC.size(); i++) {
                    Cuenta c = listaC.get(i);
                    if (c.getUsuario().equals(usuario.getCuenta().getUsuario())) {
                        listaC.get(i).setPass(txtnewpass.getText());
                        break;
                    }
                }
                Cuenta.registrar(listaC);
                Alert a = new Alert(Alert.AlertType.INFORMATION, "Debe volver a iniciar sesión");
                a.showAndWait();
                FXMLLoader fxml = App.fxmLoad("VLogin");
                App.llamarEscena(fxml, (Event) e);
            }
        } catch (EspacioVacioException esp) {
            Alert alerta = new Alert(Alert.AlertType.INFORMATION, esp.getMessage());
            alerta.show();
        }

    }

    public void setUser(Persona p) {
        this.usuario = p;
    }
}
