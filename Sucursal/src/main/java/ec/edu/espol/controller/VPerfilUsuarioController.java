/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Persona;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author USER
 */
public class VPerfilUsuarioController implements Initializable {

    @FXML
    private TextField txtusernom, txtuserape, txtusercedula, txtuserorg, txtuserperfil, txtusercorreo, txtusertype;

    @FXML
    private Button btcambiarPass;

    @FXML
    private Label lblcambiaruser;
    
    @FXML
    private Button btnvolver;

    private Persona usuario;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txtusernom.setEditable(false);
        txtuserape.setEditable(false);
        txtusercedula.setEditable(false);
        txtusercorreo.setEditable(false);
        txtuserorg.setEditable(false);
        txtusertype.setEditable(false);
        txtuserperfil.setEditable(false);
    }

    @FXML
    private void cambiarPass(MouseEvent event) throws IOException {
        FXMLLoader fxml = App.fxmLoad("VCambiarpassw");
        App.llamarEscena(fxml, (Event) event);
        VCambiarpasswordController controlador = fxml.getController();
        controlador.setUser(usuario);
    }

    @FXML
    private void cambiarUser(MouseEvent event) throws IOException {
        FXMLLoader fxml = App.fxmLoad("VcambiarUser");
        App.llamarEscena(fxml, event);
        VCambiaruserController controlador = fxml.getController();
        controlador.setUser(usuario);
    }

    @FXML
    private void regresar(MouseEvent event) throws IOException {
        FXMLLoader fxml = App.fxmLoad("VUGeneral");
        App.llamarEscena(fxml, (Event) event);
        VGeneralController controlador = fxml.getController();
        controlador.setCuenta(usuario.getCuenta());
        controlador.setear();

    }

    public void llenarTexto(String nombre, String apellido, String cedula, String organizacion, String user, String correo, String tipoUser) {
        txtusernom.setText(nombre);
        txtuserape.setText(apellido);
        txtusercedula.setText(cedula);
        txtusercorreo.setText(correo);
        txtuserorg.setText(organizacion);
        txtusertype.setText(tipoUser);
        txtuserperfil.setText(user);

    }

    public void getPersona(Persona p) {
        this.usuario = p;
    }
    }
