package ec.edu.espol.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.event.Event;
import javafx.scene.Node;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    private static String ext=".fxml";
    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("VLogin"));
        stage.setScene(scene);
        stage.setTitle("Vehiculos SEG");
        stage.show();
       
    }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }
    public static void setRoot(FXMLLoader fxml) throws IOException{
        scene.setRoot(fxml.load());
    }
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ext));
        return fxmlLoader.load();
    }
    
    public static FXMLLoader fxmLoad(String fxml) {
        return new FXMLLoader(App.class.getResource(fxml + ext));
    }
    
    public static void main(String[] args) {
        launch();
    }
    
    public static void llamarEscena(String fxml,Event event) throws IOException
    {
        Parent root = FXMLLoader.load(App.class.getResource(fxml+ ext));
        Scene scene = new Scene(root);
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.toFront();
        appStage.show();
    }
    
    public static void llamarEscena(FXMLLoader fxml,Event event) throws IOException{
        Scene scene = new Scene(fxml.load());
        Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        appStage.setScene(scene);
        appStage.toFront();
        appStage.show();
    }
}